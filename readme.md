EstouSalvo.com 
============

FOI BOM ENQUANTO DUROU...

É com um pouco de tristeza que venho dar esta notícia. O **EstouSalvo** completou seus 2 anos onde conquistou diversos blogueiros que começaram a utilizar os nossos serviços e nos deram um voto de confiança, porem nem tudo é alegria, devido aos últimos acontecimentos e com uma má estrategia adotada para manter o **EstouSalvo** funcionando decidimos que não da mais para continuar, então estaremos desviando os nossos esforços para outros projetos...

Mas tem uma boa notícia ao longo período de funcionamento recebi diversos emails pedido para fazer um agregador com as mesmas características do nosso sistema. Façam o download código fonte do **EstouSalvo** e utilizem de forma livre, porem não explicarei de que maneira é feita a instalação ou manutenção do mesmo...

Att.: [Rodolfo Silva](http://rodolfosilva.com) - <contato@rodolfosilva.com>

Requisitos não funcionais
-------------------------

### RNF-001 - Servidor ###
* Linux
* Apache 2.2+
    * Suporte para HTACCESS
    * Mod_rewrite habilitado
    * Não permitir a listagem dos diretorios
* PHP 5.4+
    * Display erros desabilitado
* HTML 5
* MySQL 5.5+
    * Suporte a triggers