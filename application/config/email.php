<?php defined('BASEPATH') or exit('No direct script access allowed');

$config['protocol']  = 'sendmail'; // protocolo de envio do email, podendo ter os valores: mail, sendmail, ou smtp
$config['charset'] = 'utf8'; // codificação de caracteres do seu email
$config['wordwrap'] = true; // TRUE/FALSE indica se deve haver a quebra de palavras ou não
$config['mailtype'] = 'html'; // tipo do email

$config['smtp_host'] = 'mail.estousalvo.com.br'; // servidor smtp
$config['smtp_port'] = 25; // usuário do smtp
$config['smtp_user'] = 'naoresponda@estousalvo.com.br'; // usuário do smtp
$config['smtp_pass'] = '0p9o8i7u6y'; // senha do smtp
$config['smtp_timeout'] = 5; // tempo de espera pela resposta do smtp

/* End of file email.php */
/* Location: ./application/config/email.php */
