<?php defined('BASEPATH') or exit('No direct script access allowed');


// ----- Padrão ----- //
$route['default_controller']                 = 'home';
$route['404_override']                       = 'Page';

// ----- Rotas de Usuário ----- //
$route['login']                              = 'users/index';
$route['login/fb']                           = 'users/authFb';
$route['registro']                           = 'users/add';
$route['logout']                             = 'users/logout';
$route['recuperar_senha']                    = 'users/recuperarSenha';
$route['reset_senha/([A-Za-z0-9]{32})']      = 'users/resetSenha/$1';
$route['conteudoAdulto']                     = $route['conteudo_adulto'] = 'home/conteudoAdulto';
$route['conteudoAdulto/(:any)']              = $route['conteudo_adulto/(:any)'] = 'home/conteudoAdulto/$1';

// ----- Rotas de Link ----- //
$route['go/([A-Za-z0-9]{5,8})?']             = 'links/redirect/$1';
$route['go/([A-Za-z0-9]{5,8})\.?']           = 'home/view/$1';
$route['rss'] = $route['feed']               = 'page/rss';
$route['page/contato']                       = 'page/contato';
$route['buscar/(:any)']                      = 'home/buscar/$1';
$route['buscar/(:any)/(:num)']               = 'home/buscar/$1/$2';
$route['page/(:any)']                        = 'page/view/$1';
$route['categoria/(:any)']                   = 'home/index/$1';
$route['categoria/(:any)/(:num)']            = 'home/index/$1//$2';
$route['categoria/(:any)/(:any)/(:num)']     = 'home/index/$1/$2/$3';
$route['ultimos_enviados']                   = 'home/ultimosEnviados';
$route['ultimos_enviados/(:num)']            = 'home/ultimosEnviados//$1';
$route['ultimos_enviados/categoria/(:any)']  = 'home/ultimosEnviados/$1';
$route['ultimos_enviados/categoria/(:any)/(:num)'] = 'home/ultimosEnviados/$1/$2';

// ----- Rotas de Load ----- //
$route['public/css/styles.css']              = 'carregar/compileCss';
$route['load/config.js']                     = 'carregar/config_js';
$route['load/images_url.json']               = 'carregar/get_images_url';
$route['load/novos_links.json']              = 'carregar/get_new_links';
$route['load/ultimas_visitas.json']          = 'carregar/get_ultimas_visitas';
$route['load/autoria_site.json']             = 'carregar/checka_autoria_site';
$route['img/thumb/([A-Za-z0-9]{32})']        = 'carregar/thumb/$1';

// ----- Rota de Administração ----- //
$route['painel/addLink']                     = $route['painel/add_link'] = 'links/add';
$route['painel/editLink/(:num)']             = $route['painel/edit_link/(:num)'] = 'links/edit/$1';
$route['painel/links']                       = 'links/index';
$route['painel/links/(:num)']                = 'links/index/$1';
$route['painel/sites']                       = 'sites/index';
$route['painel/ativarSite/(:any)']           = 'sites/ativar/$1';
$route['painel/bannerSite/(:any)']           = 'sites/banner/$1';
$route['painel/addSite']                     = $route['painel/add_site'] = 'sites/add';
$route['painel/editSite/(:num)']             = $route['painel/edit_site/(:num)'] = 'sites/edit/$1';
$route['painel/toggle_site/(:num)']          = 'sites/toogleVisualizacao/$1';
$route['painel/toggle_link/(:num)']          = 'links/toogleVisualizacao/$1';
$route['painel/configuracoesDaConta']        = $route['painel/configuracoes_da_conta'] = 'users/edit';
$route['painel/password']                    = 'users/mudarSenha';
$route['painel/novidades']                   = 'posts/index';
$route['painel/novidades/(:num)']            = 'posts/index/$1';

$route['(:num)']                             = 'home/index//$1';
