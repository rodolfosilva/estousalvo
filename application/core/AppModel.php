<?php defined('BASEPATH') or exit('No direct script access allowed');

class AppModel extends CI_Model
{
    protected $table;

    protected $default_args = array(
        'limit'      => null,
        'offset'     => null,
        'published'  => true,
        'conditions' => array(),
        'order'      => array(),
    );

    public function __construct()
    {
        parent::__construct();
        log_message('debug', 'AppModel Class Initialized');
    }

    protected function offset($limit, $page)
    {
        $page = ceil($page) - 1;
        $page = $page <= 0 ? 0 : $page;
        $offset = $page * $limit;
        return $offset < 0 ? 0 : ceil($offset);
    }

    protected function orderBy($order = array())
    {
        if (!empty($order)) {
            if (is_array($order)) {
                foreach ($order as $key => $value) {
                    if (is_numeric($key)) {
                        $this->db->order_by($value, 'ASC');
                    } else {
                        $this->db->order_by($key, $value);
                    }
                }
            } elseif (is_string($order)) {
                $this->db->order_by($order);
            }
        }
    }

    private function checkIsSetTable()
    {
        if (!isset($this->table) || empty($this->table)) {
            log_message('error', 'Table name is not defined '.$this->table);
            return false;
        }
        return true;
    }

    public function getAll($limit = null, $offset = null, $published = true, $from = null)
    {
        if ($this->checkIsSetTable()) {
            if ($limit != null && $offset != null) {
                $this->db->limit($limit, $offset);
            } elseif ($limit != null) {
                $this->db->limit($limit);
            }
            if ($published) {
                $this->db->where($this->table.'.published', true);
            }
            if (!empty($from)) {
                $this->db->from($from);
            } else {
                $this->db->from($this->table);
            }
            return $this->db->get()->result_array();
        }
        return false;
    }

    public function getById($id = null)
    {
        return (!empty($id)) ? $this->getBy($this->table.'.id', $id) : false;
    }

    public function getBy($key, $value = null)
    {
        $this->db->where($key, $value);
        $this->db->limit(1);
        $result = $this->getAll();
        return isset($result[0]) ? $result[0] : false;
    }

    public function getCount($published = true)
    {
        if ($this->checkIsSetTable()) {
            if ($published) {
                $this->db->where($this->table.'.published', true);
            }
            return $this->db->count_all_results($this->table);
        }
        return false;
    }

    public function add($args = array())
    {
        if ($this->checkIsSetTable()) {
            $this->db->insert($this->table, $args);
            return $this->db->insert_id();
        }
        return false;
    }

    public function edit($args = array(), $where = array())
    {
        if ($this->checkIsSetTable()) {
            if (is_array($where)) {
                $this->db->where($where);
            } else {
                $this->db->where($this->table.'.id', $where);
            }
            return $this->db->update($this->table, $args);
        }
        return false;
    }

    public function delete($id = false)
    {
        if ($this->checkIsSetTable() && $id) {
            $this->db->where('id', $id);
            $this->db->update($this->table, array('published' => false));
            return true;
        }
        return false;
    }

    protected function getCurUser($key = false)
    {
        if ($dados = $this->session->userdata('user_logged')) {
            if ($key === false) {
                return $dados;
            } elseif (isset($dados[$key])) {
                return $dados[$key];
            }
        }
        return false;
    }

    protected function defaultSiteImg($domain, $img)
    {
        return empty($img) ? 'http://s.wordpress.com/mshots/v1/'.urlencode('http://'.$domain).'?w=230&h=40' : $img;
    }
}

/* End of file AppModel.php */
/* Location: ./application/core/AppModel.php */
