<?php defined('BASEPATH') or exit('No direct script access allowed');

class AppRouter extends CI_Router
{
    private function stringPSR2($string)
    {
        $new_string = explode('_', $string);
        if (is_array($new_string)) {
            foreach ($new_string as $key => $value) {
                $new_string[$key] = ucfirst(strtolower($value));
            }
            return implode('', $new_string);
        } else {
            return ucfirst($string);
        }
    }

    public function _validate_request($segments)
    {
        if (count($segments) == 0) {
            return $segments;
        }

        if (isset($segments[0])) {
            $tmp = $this->stringPSR2($segments[0]);
            if (!file_exists(APPPATH.'controllers/'.$tmp.'.php')) {
                $tmp =  $segments[0];
             }
            $segments[0] = $tmp;
        }

        if (isset($segments[1])) {
            $segments[1] = $this->stringPSR2($segments[1]);
            $segments[1] = (string)(strtolower(substr($segments[1], 0, 1)).substr($segments[1], 1));
        }
        return parent::_validate_request($segments);
    }
}
