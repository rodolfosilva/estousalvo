<?php defined('BASEPATH') or exit('No direct script access allowed');

class AppController extends CI_Controller
{
    protected static $sessionStarted = false;
    protected $view = '';
    protected $data = array();

    public function __construct()
    {
        parent::__construct();
        log_message('debug', 'AppController Class Initialized');

        isset($this->Link) or $this->load->model('Link');
        isset($this->Categoria) or $this->load->model('Categoria');

        $this->twiggy->title($this->config->item('site_name'))->set_title_separator(' - ');

        $this->twiggy->set('site_name', $this->config->item('site_name'), true);
        $this->twiggy->set('site_version', $this->config->item('site_version'), true);
        $this->twiggy->set('site_description', $this->config->item('site_description'), true);
        $this->twiggy->set('novos_links', $this->Link->getNewLinks(), true);
        $this->twiggy->set('menu_categorias', $this->Categoria->getCategoriasComConteudo(), true);
        $this->twiggy->set('current_page', strtolower($this->router->fetch_class().'/'.$this->router->fetch_method()), true);

        if ($userdata = $this->getCurUser()) {
            $this->_twiggy = $this->config->item('twiggy');
            $this->twiggy->set('_CURUSER', $userdata, true);
            $this->twiggy->set('_painel', '_painel/' .$this->_twiggy['default_layout']. $this->_twiggy['template_file_ext'], true);
        }

        $this->contadorDeVisitas();
    }

    public function contadorDeVisitas()
    {
        if (!$this->input->is_ajax_request()) {
            isset($this->Visita) or $this->load->model('Visita');
            if (!$this->session->userdata('visita') || $this->session->userdata('visita') < time()) {
                $this->session->set_userdata('visita', time() + (5 * 60));
                $this->load->library('user_agent');
                $referrer = null;
                if ($this->agent->is_referral()) {
                    $referrer = get_url_domain($this->agent->referrer(), true);
                }
                if ($this->agent->is_browser()) {
                    $agent = $this->agent->browser().' :|: '.$this->agent->version();
                } elseif ($this->agent->is_robot()) {
                    $agent = $this->agent->robot();
                } elseif ($this->agent->is_mobile()) {
                    $agent = $this->agent->mobile();
                } else {
                    $agent = 'Unidentified User Agent';
                }

                $this->Visita->add(
                    array(
                        'browser' => $agent,
                        'plataforma' => $this->agent->platform(),
                        'domain' => $referrer,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'created' => date('Y-m-d H:i:s'),
                    )
                );
            }
            // $this->twiggy->set('ultimas_visitas', $this->Visita->ultimasVisitas());
        }
    }

    protected function title($str = null)
    {
        $str = trim($str);
        $this->twiggy->title()->append($str);
        $this->twiggy->set('page_title', $str);
    }

    protected function runACL($isHome = false)
    {
        if ($userdata = $this->getCurUser()) {
            /*$this->acl->init($userdata['id']);
            if (!$isHome && $this->acl->checkPermision() == false) {
                redirect('/');
            }*/
        } else {
            redirect('/login');
        }
    }

    protected function checkIsLoged()
    {
        if (!$this->getCurUser()) {
            if ($this->input->is_ajax_request()) {
                echo json_encode(
                    array(
                        // 'error' => 'Sua sessão expirou por inatividade. Para sua segurança, faça login novamente.',
                        'error' => 'A sua sessão expirou. Faça login novamente.',
                        'login' => true
                    )
                );
                exit;
            } else {
                redirect('/login');
            }
        }
    }

    protected function getCurUser($key = false)
    {
        if ($dados = $this->session->userdata('user_logged')) {
            if ($key === false) {
                return $dados;
            } elseif (isset($dados[$key])) {
                return $dados[$key];
            }
        }
        return false;
    }

    protected function arrayToList($array, $key, $value, $label_firts = false)
    {
        $rtn = array();
        if ($label_firts !== false) {
            $rtn = array(null => $label_firts);
        }
        foreach ($array as $ar) {
            $rtn[$ar[$key]] = $ar[$value];
        }
        return $rtn;
    }

    protected function configPagination($base_url = '', $limit = 15, $total_rows = 100, $uri_segment = 3, $num_links = 3, $options = array())
    {
        $config['base_url'] = site_url($base_url);
        $config['per_page'] = $limit;
        $config['total_rows'] = $total_rows;
        $config['uri_segment'] = $uri_segment;
        $config['num_links'] = $num_links;
        $config['use_page_numbers'] = true;
        $config['full_tag_open'] = '<div><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_tag_open'] = $config['last_tag_open'] = $config['next_tag_open'] = $config['num_tag_open'] = $config['prev_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close'] = $config['next_tag_close'] = $config['num_tag_close'] = $config['prev_tag_close'] = '</li>';
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $config['next_link'] = '&gt;';
        $config['prev_link'] = '&lt;';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        return array_merge($config, $options);
    }

    protected function display($view)
    {
        $this->output->set_output($this->render($view));
    }


    protected function render($view)
    {
        return $this->twiggy->render($view);
    }

    protected function set($key, $value = null, $global = false)
    {
        $this->twiggy->set($key, $value, $global);
    }

    protected function outputJson($data = array(), $cache = false)
    {
        $this->output->set_content_type('application/json');
        if ($cache) {
            $this->output->cache(24*60*60);
        }
        return $this->output->set_output(json_encode($data));
    }

    protected function enviarEmail($destiono, $assunto, $mensagem = '', $cumprimento = '')
    {
        $this->load->library('email');
        $this->email->from($this->config->item('email_replay'), $this->config->item('site_name'));
        $this->email->to($destiono);
        $this->email->subject($assunto);
        if (!is_array($mensagem)) {
            $mensagem = array(array('text' => $mensagem));
        }
        $this->email->message(
            $this->load->view(
                'layouts/email',
                array(
                    'assunto' => $assunto,
                    'mensagem' => $mensagem,
                    'cumprimento' => $cumprimento
                ),
                true
            )
        );
        return (@$this->email->send() == true);
    }
}

/* End of file AppController.php */
/* Location: ./application/core/AppController.php */
