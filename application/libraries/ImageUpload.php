<?php defined('BASEPATH') or exit('No direct script access allowed');

class ImageUpload
{
    private $ci;
    private $data = array();
    private $path = false;
    private $uploaded = false;

    public function __construct()
    {
        $this->ci =& get_instance();
        log_message('debug', 'FileUpload Class Initialized');
    }

    // private $uploaded = false;
    private function setData($arquivo, $altura, $largura, $tamanho, $tipo, $path)
    {
        $this->data = array(
            'arquivo' => $arquivo,
            'altura' => $altura,
            'largura' => $largura,
            'tamanho' => $tamanho,
            'tipo' => $tipo,
            'created' => date('Y-m-d H:i:s'),
            'diretorio' => str_replace(FCPATH, '', $path),
        );
        return $this->save();
    }

    public function getData()
    {
        return !empty($this->data) && $this->isUploaded() ? $this->data : false;
    }

    private function setId($id = null)
    {
        if (!empty($id)) {
            $this->data = array_merge($this->data, array('id' => $id));
            return true;
        } else {
            return false;
        }
    }

    // Cria o diretório
    private function mkdir($path)
    {
        $this->ci->load->helper('file');
        try {
            $index_html = '<html>'."\n".
            '<head>'."\n".
            "\t".'<title>403 Forbidden</title>'."\n".
            '</head>'."\n".
            '<body>'."\n\n".
            "\t".'<p>Directory access is forbidden.</p>'."\n\n".
            '</body>'."\n".
            '</html>';

            if (!is_dir($path)) {
                mkdir($path, 0777);
                write_file($path . 'index.html', $index_html);
            }
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    private function rmFile($file = null)
    {
        if (!empty($file) && is_string($file)) {
            if (file_exists($file)) {
                @unlink($file);
            }
        }
    }

    private function save()
    {
        $data = $this->getData();
        if ($this->isUploaded() && $data) {
            if (isset($data['id'])) {
                unset($data['id']);
            }
            isset($this->ci->Imagem) OR $this->ci->load->model('Imagem');
            if ($id = $this->ci->Imagem->add($data)) {
                $this->setId($id);
                return true;
            } else {
                $this->rmFile($this->path.$this->arquivo);
            }
        }
        return false;
    }

    public function getImg($id = null)
    {
        isset($this->ci->Imagem) OR $this->ci->load->model('Imagem');
        if (!empty($id)) {
            return $this->ci->Imagem->getById($id);
        }
        return false;
    }

    public function isUploaded()
    {
        return $this->uploaded == true;
    }

    /**
     * Return upload directory
     * @return String
     */
    private function getDirUpload()
    {
        $path = FCPATH . 'public/uploads/'.date('Y/');
        if ($this->mkdir($path)) {
            $path .= date('m/');
            if ($this->mkdir($path)) {
                $this->path = $path;
                return $path;
            }
        }
        return false;
    }

    public function fileUpload($file_name)
    {
        $this->uploaded = false;
        $file = $_FILES[$file_name];
        if ($file && isset($file['type'])) {
            switch($file['type']) {
                case 'image/png':
                case 'image/jpg':
                case 'image/jpeg':
                case 'image/gif':
                    $this->getDirUpload();
                    if ($this->path) {
                        preg_match('/\.(gif|bmp|png|jpg|jpeg){1}$/i', $file['name'], $ext);
                        if (isset($ext[1])) {
                            $name = md5(uniqid($_SERVER['REQUEST_TIME'].'-'.$file['name'])). '.' . $ext[1];
                            if (move_uploaded_file($file['tmp_name'], $this->path.$name)) {
                                if (file_exists($this->path.$name)) {
                                    $x = @filesize($this->path.$name);
                                    list($w, $h) = getimagesize($this->path.$name);
                                    $this->uploaded = true;
                                    $this->setData($name, $h, $w, $x, $file['type'], $this->path);
                                }
                            } else {
                                $this->rmFile($this->path.$name);
                            }
                        }
                    }
                    break;
            }
        }
        return $this;
    }

    /**
     * Enviar uma imagem de uma URL
     * @return
     */
    public function uploadURL($src, $max_size = 2097152, $max_width = false, $max_height = false)
    {
        $this->uploaded = false;
        $request = @getimagesize($src);
        if ($request && isset($request['mime'])) {
            switch($request['mime']) {
                case 'image/png':
                case 'image/jpg':
                case 'image/jpeg':
                case 'image/gif':
                    $this->getDirUpload();
                    if ($this->path && !(($max_width || $max_height) && (($max_width && $request[0] > $max_width) || ($max_height && $request[1] > $max_height)))) {
                        $name = md5(uniqid($_SERVER['REQUEST_TIME'].'-'.$src));
                        $ext = explode('/', $request['mime']);
                        $name .= '.'.(end($ext) == 'jpeg' ? 'jpg' : end($ext));

                        if (preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $src)) {
                            $x = array_change_key_case(get_headers($src, 1), CASE_LOWER);
                            $x = strcasecmp($x[0], 'HTTP/1.1 200 OK') != 0? $x['content-length'][1] : $x['content-length'];
                        } else {
                            $x = @filesize($src);
                        }
                        if ($x <= $max_size) {
                            file_put_contents($this->path.$name, curlGetContents($src));
                            if (file_exists($this->path.$name)) {
                                $x = @filesize($this->path.$name);
                                if ($x > $max_size) {
                                    $this->rmFile($this->path.$name);
                                } else {
                                    $this->uploaded = true;
                                    $this->setData($name, $request[0], $request[1], $x, $request['mime'], $this->path);
                                }
                            }
                        }
                    }
                    break;
            }
        }
        return $this;
    }

    public function crop($id = null, $x = 0, $y = 0, $w = 0, $h = 0, $t_w = 100, $t_h = 100)
    {
        $data = $this->getData();
        if (!empty($id) && is_numeric($id)) {
            if ($data && isset($data['id']) && $data['id'] == $id) {
                $options = $data;
            } else {
                $options = $this->getImg();
            }
        } elseif ($data) {
            $options = $data;
        }
        if (is_array($options)) {
            $name = explode('.', $options['arquivo']);
            $img_path = FCPATH.$options['diretorio'];
            $config = array(
                'source_image' => $img_path.$options['arquivo'],
                'x_axis' => $x,
                'y_axis' => $y,
                'width' => $t_w,
                'height' => $t_h,
                'orig_width' => $w,
                'orig_height' => $h,
                'quality' => 100,
                'maintain_ratio' => false,
                'new_image' => $img_path.$name[0].'-'.$t_w.'x'.$t_h.'.'.$name[1]
            );
            $this->ci->load->library('image_lib', $config);

            if ($this->ci->image_lib->jcrop($x, $y, $t_w, $t_h, $w, $h)) {
                isset($this->ci->Imagem) OR $this->ci->load->model('Imagem');
                $this->ci->Imagem->addOrChangeOption(
                    array(
                        'crop' => array(
                            $t_w.'x'.$t_h => array(
                                'x' => $x,
                                'y' => $y,
                                'w' => $w,
                                'h' => $h,
                                't_w' => $t_w,
                                't_h' => $t_h,
                            )
                        )
                    ),
                    $options['id']
                );
                return true;
            }
            return false;
        }
    }
}

/* End of file ImageUpload.php */
/* Location: ./application/libraries/ImageUpload.php */
