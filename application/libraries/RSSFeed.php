<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * This class has methods for making a RSS 2.0 feed.
 * @author David Laurell <david.laurell@gmail.com>
 */
class RSSFeed
{
    private $ci;
    private $xml;

    /**
     * Construct a RSS feed
     * @param $title the title of the feed
     * @param $link link to the website where you can find the RSS feed
     * @param $description a description of the RSS feed
     * @param $rsslink the link to this RSS feed
     */
    public function __construct($params = array())
    {
        extract(
            array_merge(
                array(
                    'title' => '',
                    'link' => '',
                    'description' => '',
                    'rsslink' => ''
                ),
                $params
            )
        );
        $this->ci =& get_instance();

        $template  = '<?xml version="1.0" encoding="UTF-8" ?>';
        $template .= '<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/">';
        $template .= '<channel>';
        $template .= '</channel>';
        $template .= '</rss>';

        $this->xml = new SimpleXMLElement($template);
        $atomlink = $this->xml->channel->addChild('atom:link', '', 'http://www.w3.org/2005/Atom');
        $atomlink->addAttribute('href', $rsslink);
        $atomlink->addAttribute('rel', 'self');
        $atomlink->addAttribute('type', 'application/rss+xml');

        $this->xml->channel->addChild('title', $title);
        $this->xml->channel->addChild('link', $link);
        $this->xml->channel->addChild('description', $description);

        !isset($language) or $this->setLanguage($language);
    }

    /**
     * Set the language of the RSS feed
     * @param $lang the language of the RSS feed
     */
    public function setLanguage($lang)
    {
        $this->xml->channel->addChild('language', $lang);
    }

    /**
     * Adds a picture to the RSS feed
     * @param $url URL to the image
     * @param $title The image title. Usually same as the RSS feed's title
     * @param $link Where the image should link to. Usually same as the RSS feed's link
     */
    public function setImage($url, $title, $link)
    {
            $image = $this->xml->channel->addChild('image');
            $image->addChild('url', $url);
            $image->addChild('title', $title);
            $image->addChild('link', $link);
    }

    /**
     * Add a item to the RSS feed
     * @param $title The title of the RSS feed
     * @param $link Link to the item's url
     * @param $description The description of the item
     * @param $author The author who wrote this item
     * @param $guid Unique ID for this post
     * @param $timestamp Unix timestamp for making a date
     */
    public function addItem($title, $link = null, $description = null, $categoria = null, $author = null, $guid = null, $timestamp = null)
    {
        $item = $this->xml->channel->addChild('item');
        $item->addChild('title', $title);
        empty($description) or $item->addChild('description', htmlspecialchars($description));
        empty($categoria) or $item->addChild('category', $categoria);
        empty($link) or $item->addChild('link', $link);
        empty($guid) or $item->addChild('guid', $guid);
        empty($author) or $item->addChild('author', $author);
        empty($timestamp) or $item->addChild('pubDate', date('r', $timestamp));
    }

    /**
     * Displays the RSS feed
     */
    public function displayXML($return = false, $cache = false)
    {
        $xml = $this->xml->asXML();
        if ($return) {
            return $xml;
        }
        $this->ci->output->set_content_type('application/rss+xml');
        !$cache or $this->ci->output->cache($cache);
        $this->ci->output->set_header('Content-Type: application/xml; charset=utf-8');
        $this->ci->output->set_output($xml);
    }
}


/* End of file RSSFeed.php */
/* Location: ./application/libraries/RSSFeed.php */
