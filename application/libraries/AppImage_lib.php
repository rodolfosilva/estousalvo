<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AppImage_lib extends CI_Image_lib
{
    /**
     * Constructor
     *
     * Simply determines whether the template library exists.
     *
     */
	public function __construct($props = array())
	{
		parent::__construct($props);
        log_message('debug', 'Jcrop Class Initialized');
    }

	// --------------------------------------------------------------------

	/**
	 * Image Crop
	 *
	 * This is a wrapper function that chooses the proper
	 * cropping function based on the protocol specified
	 *
	 * @return	bool
	 */
	public function jcrop($x, $y, $t_w, $t_h, $w, $h)
	{
		$v2_override = false;

		// If the target width/height match the source, AND if the new file name is not equal to the old file name
		// we'll simply make a copy of the original with the new name... assuming dynamic rendering is off.
		if ($this->dynamic_output === false && $this->orig_width === $this->width && $this->orig_height === $this->height) {
			if ($this->source_image !== $this->new_image && @copy($this->full_src_path, $this->full_dst_path)) {
				@chmod($this->full_dst_path, FILE_WRITE_MODE);
			}

			return true;
		}

		// GD 2.0 has a cropping bug so we'll test for it
		if ($this->gd_version() !== false) {
			$gd_version = str_replace('0', '', $this->gd_version());
			$v2_override = ($gd_version === 2);
		}

		//  Create the image handle
		if (!($src_img = $this->image_create_gd())) {
			return false;
		}

		/* Create the image
		 *
		 * Old conditional which users report cause problems with shared GD libs who report themselves as "2.0 or greater"
		 * it appears that this is no longer the issue that it was in 2004, so we've removed it, retaining it in the comment
		 * below should that ever prove inaccurate.
		 */
		if (function_exists('imagecreatetruecolor')) {
			$create	= 'imagecreatetruecolor';
			$copy	= 'imagecopyresampled';
		} else {
			$create	= 'imagecreate';
			$copy	= 'imagecopyresized';
		}

		$dst_img = $create($t_w, $t_h);

		if ($this->image_type === 3) { // png we can actually preserve transparency
			imagealphablending($dst_img, false);
			imagesavealpha($dst_img, true);
		}

		$copy($dst_img, $src_img, 0, 0, $x, $y, $t_w, $t_h, $w, $h);

		// Show the image
		if ($this->dynamic_output === true) {
			$this->image_display_gd($dst_img);
		} elseif (!$this->image_save_gd($dst_img)) { // Or save it
			return false;
		}

		// Kill the file handles
		imagedestroy($dst_img);
		imagedestroy($src_img);

		// Set the file to 666
		@chmod($this->full_dst_path, FILE_WRITE_MODE);

		return true;
	}
}

