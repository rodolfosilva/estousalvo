<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
* CodeIgniter Template Class
*
* Library templates for CodeIgniter
*
* @package CodeIgniter
* @category Libraries
* @author SlotSYS (www.slotsys.com), Rodolfo Silva (www.rodolfosilva.com)
* @link http://github.com/slotsys/CodeIgniter/
*/
class Template
{
    private $CI;
    private $title          = '';
    private $desc           = '';
    private $script_block_b = '';
    private $script_block_a = '';
    private $layout         = false;
    private $compress       = false;
    private $last_position  = null;
    private $variaveis      = array();
    private $source         = array();

    public function __construct ()
    {
        log_message('debug', 'Class Template inicializada');
        $this->CI            =& get_instance();
        $this->layout        = $this->CI->config->item('tpl_layout');
        $this->layout        = $this->layout ? $this->layout : false;
        $this->compress      = $this->CI->config->item('tpl_compress') == true;
        $this->title         = $this->CI->config->item('tpl_title');
        $this->title         = $this->title ? $this->title : '';
        $this->desc          = $this->CI->config->item('tpl_desc');
        $this->desc          = $this->desc ? $this->desc : '';
    }

    public function set ($key, $value = null)
    {
        if (is_array($key)) {
            $this->variaveis = empty($this->variaveis) ? $key : array_merge($this->variaveis, $key);
        } else {
            $this->variaveis[ $key ] = $value;
        }
    }

    public function get ($key = null)
    {
        if ($key !== null) {
            return isset($this->variaveis[ $key ]) ? $t0his->variaveis[ $key ] : null;
        }
        return $this->variaveis;
    }

    private function parseAttributes ($options, $default = array())
    {
        $options = array_merge($default, $options);
        $attributes = '';
        foreach ($options as $key => $value) {
            $key2 = strtolower($key);
            if ($key2 == 'src' || $key2 == 'href') {
                $value = $this->CI->config->base_url($value);
            }
            $attributes .= sprintf(' %s="%s"', $key, trim(!is_array($value) ? $value : ''));
        }
        return $attributes;
    }

    private function sortSource ()
    {
        $total = count($this->source);
        for ($x = 0; $x < $total; $x++) {
            for ($y = $x; $y < $total; $y++) {
                if ($this->source[ $x ][ 'position' ] > $this->source[ $y ][ 'position' ]) {
                    $tmp         = $this->source[ $y ];
                    $this->source[ $y ] = $this->source[ $x ];
                    $this->source[ $x ] = $tmp;
                }
            }
        }
    }

    private function checkPosition ($position = 0, $return = false)
    {
        foreach ($this->source as $source) {
            if ($source['position'] == $position) {
                return $return ? $value : true;
            }
        }
        return false;
    }

    private function setSource ($tipo, $source, $position = false)
    {
        if ($position === false) {
            $position = $this->last_position;
            $this->last_position += 1;
        }
        $position = ceil($position);
        $disponivel = false;
        do {
            if ($this->checkPosition($position)) {
                $position = $position < 0 ? $position-1 : $position+1;
            } else {
                $disponivel = true;
            }
        } while ($disponivel == false);
        $this->source[] = array(
            'tipo'     => $tipo,
            'source'   => $source,
            'position' => $position,
        );
    }

    public function link ($href = '', $options = array(), $inline = true, $position = false)
    {
        $options = is_array($options) ? $options : array();

        if (is_array($href)) {
            $options = array_merge($href, $options);
        } else {
            $options['href'] = $href;
        }

        if ($inline !== true) {
            $this->setSource('link', $options, $position);
        } else {
            return sprintf(
                '<link%s/> ',
                $this->parseAttributes(
                    $options,
                    array(
                        'href' => ''
                    )
                )
            );
        }
    }

    public function compress ($html = '')
    {
        return preg_replace(
            array(
                '/\n/',         // replace end of line by a space
                '/\>[^\S ]+/s', // strip whitespaces after tags, except space
                '/[^\S ]+\</s', // strip whitespaces before tags, except space
                '/(\s)+/s',     // shorten multiple whitespace sequences
                '#(?://)?<!\[CDATA\[(.*?)(?://)?\]\]>#s', //leave CDATA alone
                '/<!--[^\[](.|\s)*?[^\]]-->/' //strip HTML comments
            ),
            array(
                '',
                '>',
                '<',
                '\\1',
                "//<![CDATA[\n".'\1'."\n//]]>",
                ''
            ),
            $html
        );
    }

    public function css($href = '', $options = array(), $inline = true)
    {
        $options = is_array($options) ? $options : array('href' => '', 'rel' => 'stylesheet', 'type' => 'text/css', 'media' => 'all');
        return $this->link($href, $options, $inline);
    }

    public function meta ($options = array(), $inline = true)
    {
        $options = is_array($options) ? $options : array();

        if ($inline !== true) {
            $this->setSource('meta', $options, $position);
        } else {
            return sprintf('<meta%s/>', $this->parseAttributes($options));
        }
    }

    public function script($src = '', $options = array(), $inline = true, $position = false)
    {
        $options = is_array($options) ? $options : array();

        if (is_array($src)) {
            $options = array_merge($src, $options);
        } else {
            $options['src'] = $src;
        }

        if ($inline !== true) {
            $this->setSource('script', $options, $position);
        } else {
            return sprintf(
                '<script%s></script>',
                $this->parseAttributes(
                    $options,
                    array(
                        'src' => '',
                        'type' => 'text/javascript'
                    )
                )
            );
        }
    }

    public function scriptBlockBefore($source)
    {
        $this->script_block_b = $source;
    }

    public function scriptBlockAfter($source)
    {
        $this->script_block_a = $source;
    }

    public function scriptBlock ($source = '', $inline = true, $position = false)
    {
        if ($inline !== true) {
            $this->setSource('script_block', $source, $position);
        } else {
            return sprintf(
                '<script type="text/javascript">'. "\n".'//<![CDATA['."\n".'%s'."\n".'%s'."\n".'%s'."\n" .'//]]>'."\n".'</script>'."\n",
                $this->script_block_b,
                trim($source),
                $this->script_block_a
            );
        }
    }

    public function styleBlock ($source = '', $inline = true, $position = false)
    {
        if ($inline !== true) {
            $this->setSource('style_block', $source, $position);
        } else {
            return sprintf(
                '<style type="text/css">'. "\n".'%s'."\n".'</style>'."\n",
                trim($source)
            );
        }
    }

    public function layout($layout)
    {
        $this->layout = trim($layout);
    }

    public function title($title)
    {
        $this->title = trim($title);
    }

    public function desc($desc)
    {
        $this->desc = trim($desc);
    }

    public function load($view = '', $view_data = array(), $return = false, $cache = false, $layout = -999)
    {
        $output = '';
        $this->set(is_array($view_data) ? $view_data : array());
        $this->layout($layout !== -999 ? $layout : $this->layout);
        $this->last_position = 10000;
        $this->contents = $this->CI->load->view($view, $this->get(), true);

        if (empty($this->layout)) {
            $output = $this->fetch('contents');
        } else {
            $this->last_position = null;
            $output = $this->CI->load->view($this->layout, $this->get(), true);
        }
        $output = $this->compress ? $this->compress($output) : $output;
        if ($return) {
            return $output;
        } else {
            $this->CI->output->set_content_type('text/html');
            if ($cache) {
                $this->CI->output->cache($cache);
            }
            return $this->CI->output->set_output($output);
        }
    }

    private function getSourceFormated($tipo)
    {
        $sources = array();
        foreach ($this->source as $source) {
            if ($source['tipo'] == $tipo) {
                $sources[] = $source['source'];
            }
        }

        $output = '';
        switch($tipo) {
            case 'meta':
            case 'link':
            case 'css':
            case 'script':
                foreach ($sources as $source) {
                    $output .= "\r\n\t".$this->$tipo($source);
                }
                break;
            case 'script_block':
                foreach ($sources as $source) {
                    $output .= $source;
                    $output .= "\n\n" .'/*--------------------------------------------*/'."\n\n";
                }
                $output = !empty($output) ? $this->scriptBlock($output) : '';
                break;
            case 'style_block':
                foreach ($sources as $source) {
                    $output .= $source;
                    $output .= "\n\n" .'/*--------------------------------------------*/'."\n\n";
                }
                $output = !empty($output) ? $this->styleBlock($output) : '';
                break;
        }
        return $output."\r\n";
    }

    public function fetch($key = '')
    {
        $output = '';
        switch($key) {
            case 'title':
            case 'desc':
            case 'contents':
                $output = $this->$key;
                break;
            case 'links':
                $this->sortSource();
                $output = $this->getSourceFormated('link');
                break;
            case 'scripts':
                $this->sortSource();
                $output = $this->getSourceFormated('script');
                break;
            case 'script_block':
            case 'script_style':
                $this->sortSource();
                $output = $this->getSourceFormated($key);
                break;
        }
        return $output;
    }
}

/* End of file Template.php */
/* Location: ./application/libraries/Template.php */
