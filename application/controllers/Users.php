<?php defined('BASEPATH') or exit('No direct script access allowed');

class Users extends AppController
{
    private $facebook_scope = array();
    public function __construct()
    {
        parent::__construct();
        $this->load->library(
            'Facebook',
            array(
                'appId' => $this->config->item('fb_app_id'),
                'secret' => $this->config->item('fb_app_secret'),
            )
        );
        $this->facebook_scope = array_merge(array('redirect_uri' => site_url('login/fb')), $this->config->item('fb_app_scope'));
        if ($this->router->fetch_method() == 'edit' || strtolower($this->router->fetch_method()) == 'mudarsenha') {
            $this->checkIsLoged();
        }
        isset($this->User) or $this->load->model('User');
    }

    public function index()
    {
        if ($this->getCurUser()) {
            if ($this->input->is_ajax_request()) {
                $msg['success'] = 'Você esta logado aguarde...';
                $msg['redirect'] = site_url('/painel/novidades');
                $this->outputJson($msg);
            } else {
                redirect('/painel/novidades');
            }
        }
        $this->title('Login');
        if ($this->input->post()) {

            $username = trim($this->input->post('username'));
            $password = trim($this->input->post('password'));

            $msg = array();
            if (empty($username) && empty($password)) {
                $msg['error'] = 'Digite seu email ou nome de usuário e sua senha.';
            } elseif (empty($username)) {
                $msg['error'] = 'Digite seu nome de usuário ou seu endereço de email no formato nome@example.com.';
            } elseif (empty($password)) {
                $msg['error'] = 'Digite sua senha.';
            } elseif ($this->doLogin($username, $password)) {
                //$this->User->setUserSession($query['id']);
                $msg['success'] = 'Você esta logado aguarde...';
                if ($this->input->post('logado')) {
                    $msg['close_modal'] = true;
                } else {
                    $msg['redirect'] = site_url('/painel/novidades');
                }
            } else {
                $msg['error'] = 'E-mail ou senha inválido, tente novamente.';
            }
            $this->outputJson($msg);
        } else {
            $this->set('facebook_login_url', $this->facebook->getLoginUrl($this->facebook_scope));
            $this->display('users/login');
        }
    }

    public function logout()
    {
        if (!$this->getCurUser()) {
            redirect('/');
        }
        $this->session->sess_destroy();
        redirect('/');
    }

    public function add()
    {
        if ($this->input->post()) {
            $nome           = trim($this->input->post('nome'));
            $email          = trim($this->input->post('email'));
            $usuario        = trim($this->input->post('usuario'));
            $senha          = trim($this->input->post('senha'));
            $repete_senha   = trim($this->input->post('repete_senha'));
            $blog           = trim($this->input->post('blog'));
            $msg = array();
            if (empty($nome)) {
                $msg['error'] = 'Digite seu nome completo.';
            } elseif (empty($email)) {
                $msg['error'] = 'Digite seu endereço de email no formato nome@example.com.';
            } elseif (!preg_match('/^[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\-]+\.[a-z]{2,4}$/', $email)) {
                $msg['error'] = 'E-mail inválido, digite seu endereço de email no formato nome@example.com.';
            } elseif ($this->User->uniqueByEmail($email) >= 1) {
                $msg['error'] = 'Este email já esta sendo usado por outro usuário.';
            } elseif (empty($usuario)) {
                $msg['error'] = 'Digite seu nome de usuário.';
            } elseif (!preg_match('/^([A-Z,a-z,0-9,_,-]){2,}/', $usuario)) {
                $msg['error'] = 'Nome de usuário inválido, digite um nome de usuário válido.';
            } elseif (empty($senha)) {
                $msg['error'] = 'Digite sua senha.';
            } elseif ($senha <= '5') {
                $msg['error'] = 'Sua senha é muito curta, digite uma senha maior que 5.';
            } elseif (empty($repete_senha)) {
                $msg['error'] = 'Repita sua senha.';
            } elseif ($repete_senha != $senha) {
                $msg['error'] = 'As senhas são diferentes, redigite sua senha.';
            } elseif ($this->User->uniqueByUsername($usuario) >= 1) {
                $msg['error'] = 'Este nome de usuário não esta disponivel.';
            } else {
                if ($id = $this->User->add(array('nome' => $nome, 'email' => $email, 'username' => $usuario, 'password' => $this->User->encript($senha), 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s')))) {
                    $this->doLogin($usuario, $senha);
                    $msg['success'] = 'Sua conta foi criada com sucesso.';
                    $msg['redirect'] = site_url('/');
                    if ($this->enviarEmail(
                        $email,
                        'Seja Bem-Vindo',
                        array(
                            array('text' => 'Olá, <strong>'.$nome.'</strong>. Obrigado por se cadastrar no <strong>'.$this->config->item('site_name').'</strong>, um dos melhores agregadores da web.'),
                            array('text' => 'Acesse seu painel, cadastre seus sites e comece a enviar ou agendar a publicação de links.'),
                            array('link' => site_url('/login')),
                        ),
                        'Seja Bem-Vindo ao '.$this->config->item('site_name').'!'
                    )) {
                        $msg['success'] .= 'Foi enviado um email para você.';
                    }
                } else {
                    $msg['error'] = 'Erro ao tentar criar a sua conta.';
                }
            }
            $this->outputJson($msg);
        } else {
            $this->set('facebook_login_url', $this->facebook->getLoginUrl($this->facebook_scope));
            $this->display('users/registro');
        }
    }

    public function resetSenha($token = null)
    {
        if ($this->input->post()) {
            if (trim($this->input->post('token')) == trim($token)) {
                $senha          = trim($this->input->post('senha'));
                $repete_senha   = trim($this->input->post('repete_senha'));
                $token          = trim($this->input->post('token'));
                $msg = array();

                if (empty($senha)) {
                    $msg['error'] = 'Digite sua senha.';
                } elseif ($senha <= '5') {
                    $msg['error'] = 'Sua senha é muito curta, digite uma senha maior que 5.';
                } elseif (empty($repete_senha)) {
                    $msg['error'] = 'Repita sua senha.';
                } elseif ($repete_senha != $senha) {
                } elseif (!$this->User->getByToken($token)) {
                    $msg['error'] = 'As senhas são diferentes, redigite sua senha.';
                } else {
                    if ($id = $this->User->edit(array('password' => $this->User->encript($senha)), array('token' => $token))) {
                        $msg['success'] = 'Sua nova senha foi cadastrada com sucesso.';
                        $msg['redirect'] = site_url('/');
                    } else {
                        $msg['error'] = 'Erro ao tentar cadastrar a nova senha.';
                    }
                }
            } else {
                $msg['error'] = 'Token invalido.';
            }
            $this->outputJson($msg);
        } else {
            if ($this->User->getByToken($token)) {
                $this->set('token', $token);
                $this->display('users/reset_password');
            } else {
                show_404();
                exit();
            }
        }
    }

    public function recuperarSenha()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $msg = array();
            $email = trim($this->input->post('email'));
            $usuario = trim($this->input->post('usuario'));
            if ($result = $this->User->resetarSenha($usuario, $email)) {
                $link_reset = site_url('reset_senha/'.$result['token']);
                if ($this->enviarEmail(
                    $result['email'],
                    'Recuperar senha',
                    array(
                        array('text' => 'Recebemos uma solicita&#231;&#227;o para redefini&#231;&#227;o de senha da sua conta. Para prosseguir, clique no link abaixo:'),
                        array('link' => $link_reset),
                        array('text' => 'Caso o link n&#227;o funcione, acesse direto copiando e colando na barra de navega&#231;&#227;o do seu navegador.'),
                        array('text' => '<em>Este email foi enviado por algu&#233;m que pediu para alterar a senha da sua conta no <strong>EstouSalvo</strong>. Se esta solicita&#231;&#227;o n&#227;o foi realizada por voc&#234; ignore esta mensagem.</em>'),
                    ),
                    'Olá, '.$result['nome']
                )) {
                    $msg['success'] = 'Foi enviado uma mensagem para sua conta de email, cheque sua caixa de entrada, ou sua caixa de span.';
                } else {
                    $msg['error'] = 'Ocorreu um erro ao enviar o email, tente novamente mais tarde.';
                }
            } else {
                $msg['error'] = 'Usuário ou email não encontrado.';
            }
            $this->outputJson($msg);
        } else {
            $this->display('users/recuperar_senha');
        }
    }

    public function edit()
    {
        if ($this->input->post()) {
            $nome           = trim($this->input->post('nome'));
            $email          = trim($this->input->post('email'));
            $usuario        = trim($this->input->post('usuario'));
            $msg = array();
            if (empty($nome)) {
                $msg['error'] = 'Digite seu nome completo.';
            } elseif (empty($email)) {
                $msg['error'] = 'Digite seu endereço de email no formato nome@example.com.';
            } elseif (!preg_match('/^[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\-]+\.[a-z]{2,4}$/', $email)) {
                $msg['error'] = 'E-mail inválido, digite seu endereço de email no formato nome@example.com.';
            } elseif ($this->User->uniqueByEmail($email, $this->getCurUser('id')) >= 1) {
                $msg['error'] = 'Este email já esta sendo usado por outro usuário.';
            } elseif (empty($usuario)) {
                $msg['error'] = 'Digite seu nome de usuário.';
            } elseif (!preg_match('/^([A-Z,a-z,0-9,_,-]){2,}/', $usuario)) {
                $msg['error'] = 'Nome de usuário inválido, digite um nome de usuário válido.';
            } elseif ($this->User->uniqueByUsername($usuario, $this->getCurUser('id')) >= 1) {
                $msg['error'] = 'Este nome de usuário não esta disponivel.';
            } else {
                if ($this->User->edit(array('nome' => $nome, 'email' => $email, 'username' => $usuario, 'modified' => date('Y-m-d H:i:s')), $this->getCurUser('id'))) {
                    $msg['success'] = 'Os dados da sua conta foi alterado com sucesso.';
                } else {
                    $msg['error'] = 'Erro ao tentar alterar os dados da sua conta.';
                }
            }
            $this->outputJson($msg);
        } else {
            $this->set('user', $this->User->getById($this->getCurUser('id')));
            $this->display('users/edit');
        }
    }

    public function mudarSenha()
    {
        if ($this->input->post()) {
            $senha_atual    = trim($this->input->post('senha_atual'));
            $senha          = trim($this->input->post('senha'));
            $repete_senha   = trim($this->input->post('repete_senha'));
            $msg = array();
            if (empty($senha_atual)) {
                $msg['error'] = 'Digite sua senha atual.';
            } elseif (empty($senha)) {
                $msg['error'] = 'Digite sua nova senha.';
            } elseif ($senha <= '5') {
                $msg['error'] = 'Sua nova senha é muito curta, digite uma senha maior que 5.';
            } elseif (empty($repete_senha)) {
                $msg['error'] = 'Repita sua nova senha.';
            } elseif ($repete_senha != $senha) {
                $msg['error'] = 'As senhas são diferentes, redigite sua nova senha.';
            } else {
                $user = $this->User->getById($this->getCurUser('id'));
                if ($user['password'] != $this->User->encript($senha_atual)) {
                    $msg['error'] = 'Senha atual invalida.';
                } elseif ($this->User->edit(array('password' => $this->User->encript($senha), 'modified' => date('Y-m-d H:i:s')), $this->getCurUser('id'))) {
                    $msg['success'] = 'Sua senha foi alterado com sucesso.';
                } else {
                    $msg['error'] = 'Erro ao tentar alterar sua senha.';
                }
            }
            $this->outputJson($msg);
        } else {
            $this->display('users/password');
        }
    }

    private function doLogin($username, $password)
    {
        if ($query = $this->User->checkUser($username, $password)) {
            //$query['last_verif'] = $_SERVER['REQUEST_TIME'] + (5*60);
            return $this->setSessionLogin($query);
        }
        return false;
    }

    private function setSessionLogin($data = array())
    {
        if (is_array($data) && !empty($data) && isset($data['id'])) {
            $this->load->model('Site');
            $data['sites'] = $this->Site->getSitesByUser($data['id']);
            $this->session->set_userdata(array('user_logged' => $data));
            return true;
        }
        return false;
    }

    public function authFb()
    {
        try {
            if ($this->facebook->getUser()) {
                $data = $this->facebook->api('/me');
                if (!isset($data['email']) || empty($data['email'])) {
                    throw new Exception('O e-mail é obrigatorio.');
                } else {
                    if (($user = $this->User->getByFacebookId($data['id'])) && $this->setSessionLogin($user)) {
                        redirect('/painel/novidades');
                    } elseif ($user = $this->User->getByEmail($data['email'])) {
                        $this->User->edit(array('facebook_id' => $data['id'], 'modified' => date('Y-m-d H:i:s')), $user['id']);
                        $this->setSessionLogin($user);
                        redirect('/painel/novidades');
                    } else {
                        $new_username = '';
                        if ($this->User->uniqueByUsername($data['username']) < 1) {
                            $new_username = $data['username'];
                        } elseif ($this->User->uniqueByUsername($data['id'])) {
                            $new_username = $data['id'];
                        } else {
                            do {
                                $tmp_username = $data['username'].rand(99, 9999);
                                if ($this->User->uniqueByUsername($tmp_username) < 1) {
                                    $new_username = $tmp_username;
                                }
                            } while ($new_username == '');
                        }
                        $token = md5(uniqid(mt_rand(), true));
                        $id = $this->User->add(
                            array(
                                'nome' => $data['name'],
                                'email' => $data['email'],
                                'username' => $new_username,
                                'facebook_id' => $data['id'],
                                'token' => $token,
                                'password' => $this->User->encript($new_username),
                                'created' => date('Y-m-d H:i:s'),
                                'modified' => date('Y-m-d H:i:s')
                            )
                        );
                        $this->setSessionLogin(array('id' => $id, 'nome' => $data['name'], 'username' => $new_username, 'email' => $data['email']));
                        $this->enviarEmail(
                            $data['email'],
                            'Seja Bem-Vindo',
                            array(
                                array('text' => 'Olá, <strong>'.$data['name'].'</strong>. Obrigado por se cadastrar no <strong>'.$this->config->item('site_name').'</strong>, um dos melhores agregadores da web.'),
                                array('text' => 'Acesse seu painel, cadastre seus sites e comece a enviar ou agendar a publicação de links.'),
                                array('link' => site_url('/login')),
                            ),
                            'Seja Bem-Vindo ao '.$this->config->item('site_name').'!'
                        );
                        redirect('reset_senha/'.$token);
                    }
                }
            } else {
                throw new Exception('Você não esta logado, tente novamente');
            }
        } catch (Exception $e) {
            redirect('login');
        }
    }
}

/* End of file Users.php */
/* Location: ./application/modules/Users/controllers/Users.php */
