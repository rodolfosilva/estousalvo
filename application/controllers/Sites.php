<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sites extends AppController
{
    private $banners = array();
    public function __construct()
    {
        parent::__construct();
        $this->checkIsLoged();
        $this->banners = array(
            array(
                'url' => site_url('public/banners/468x60.jpg'),
                'descricao' => 'EstouSalvo - Agregador de Links. Inovador, Interativo, Imediato',
                'size' => '468x60'
            ),
            array(
                'url' => site_url('public/banners/120x60.jpg'),
                'descricao' => 'EstouSalvo - Agregador de Links. Inovador, Interativo, Imediato',
                'size' => '120x60'
            ),
            array(
                'url' => site_url('public/banners/150x150.jpg'),
                'descricao' => 'EstouSalvo - Agregador de Links. Inovador, Interativo, Imediato',
                'size' => '150x150'
            ),
            array(
                'url' => site_url('public/banners/80x80.jpg'),
                'descricao' => 'EstouSalvo - Agregador de Links. Inovador, Interativo, Imediato',
                'size' => '80x80'
            ),
            array(
                'url' => site_url('public/banners/270x40.jpg'),
                'descricao' => 'EstouSalvo - Agregador de Links. Inovador, Interativo, Imediato',
                'size' => '270x40'
            ),
            array(
                'url' => site_url('public/banners/250x250.jpg'),
                'descricao' => 'EstouSalvo - Agregador de Links. Inovador, Interativo, Imediato',
                'size' => '250x250'
            ),
        );
        isset($this->Site) or $this->load->model('Site');
    }

    public function index()
    {
        $this->set('sites', $this->Site->getSitesByUser($this->getCurUser('id')));
        $this->display('sites/index');
    }

    public function ativarBanner($token)
    {
        $this->set('site', $this->Site->getByToken($token));
        $this->set('banners', $this->banners);
    }

    public function ativar($token)
    {
        $this->ativarBanner($token);
        $this->display('sites/ativar');
    }

    public function banner($token)
    {
        $this->ativarBanner($token);
        $this->display('sites/banner');
    }

    public function add()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $titulo = trim($this->input->post('titulo'));
            $url = get_url_domain(trim($this->input->post('url')));

            $msg = array();
            if (empty($titulo)) {
                $msg['error'] = 'Digite o titulo do seu site.';
            } elseif (empty($url)) {
                $msg['error'] = 'Digite o endereço so seu site.';
            } elseif (!validar_url('http://'.$url)) {
                $msg['error'] = 'O domain do seu site é invalido, digite sem o http.';
            } elseif ($this->Site->addNewSite($this->getCurUser('id'), $url, htmlspecialchars($titulo, ENT_QUOTES))) {
                if ($userdata = $this->getCurUser()) {
                    $userdata['sites'] = $this->Site->getSitesByUser($this->getCurUser('id'));
                    $this->session->set_userdata(array('user_logged' => $userdata));
                }
                $this->db->where('sites.url', $url);
                $this->db->limit(1);
                $site = $this->Site->getSitesByUser($this->getCurUser('id'));
                if (isset($site[0])) {
                    $msg['success'] = 'Site cadastrado com sucesso. Aguarde...';
                    $msg['redirect'] = site_url('painel/ativarSite/'.$site[0]['token']);
                } else {
                    $msg['success'] = 'Site cadastrado com sucesso. Aguarde...';
                    $msg['redirect'] = site_url('/painel/sites');
                }
            } else {
                $msg['error'] = 'Erro ao cadastrar seu site.';
            }
            $this->outputJson($msg);
        } else {
            $this->display('sites/add');
        }
    }

    public function toogleVisualizacao($id)
    {
        $current_data = $this->Site->getById($id);
        $acao = $current_data['published'] == 1 ? 'liberar' : 'bloquear';
        if (isset($current_data['user_id']) && $current_data['user_id'] == $this->getCurUser('id') && !empty($id)) {
            if ($this->db->query('UPDATE sites SET published = IF(published=1, 2, 1) WHERE id = ? AND published >= 1', array($id))) {
                $msg['success'] = 'Visualização '.$acao.'.';
            } else {
                $msg['error'] = 'Erro ao '.$acao.' a visualização.';
            }
        } else {
            $msg['error'] = 'Você não tem permissão para '.$acao.' a visualização deste site.';
        }
        if ($this->input->is_ajax_request()) {
            $this->outputJson($msg);
        } else {
            redirect('/painel/sites');
        }
    }

    public function edit($id)
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $id = trim($this->input->post('id'));
            $titulo = trim($this->input->post('titulo'));
            $url = get_url_domain(trim($this->input->post('url')));

            $msg = array();
            if (empty($titulo)) {
                $msg['error'] = 'Digite o titulo do seu site.';
            } elseif (empty($url)) {
                $msg['error'] = 'Digite o endereço so seu site.';
            } elseif (!validar_url('http://'.$url)) {
                $msg['error'] = 'O domain do seu site é invalido, digite sem o http.';
            } else {
                $current_data = $this->Site->getById($id);
                if (isset($current_data['user_id']) && $current_data['user_id'] == $this->getCurUser('id') && $id > 0) {
                    $options = array(
                        'titulo' => $titulo,
                        'url' => $url,
                        'modified' => date('Y-m-d H:i:s')
                    );
                    if (isset($_FILES['banner']) && !empty($_FILES['banner']['name'])) {
                        $banner = $_FILES['banner'];
                        $largura = 230;
                        $altura = 40;
                        $tamanho = 1000;

                        // Verifica se o arquivo é uma imagem
                        if (!preg_match('/^image\/(pjpeg|jpeg|png|gif|bmp)$/', $banner['type'])) {
                            $msg['error'] = 'O arquivo não é uma imagem.';
                        }
                        // Pega as dimensões da imagem
                        $dimensoes = getimagesize($banner['tmp_name']);
                        // Verifica se a largura da imagem é maior que a largura permitida
                        if ($dimensoes[0] > $largura) {
                            $msg['error'] = 'A largura da imagem não deve ultrapassar '.$largura.' pixels';
                        } elseif ($dimensoes[1] > $altura) {
                            $msg['error'] = 'Altura da imagem não deve ultrapassar '.$altura.' pixels';
                        } elseif ($banner['size'] > ($tamanho * 1024)) {
                            $msg['error'] = 'A imagem deve ter no máximo '.$tamanho.' bytes';
                        } elseif (!isset($msg['error'])) {
                            isset($this->ImageUpload) or $this->load->library('ImageUpload', null, 'ImageUpload');
                            $upload = $this->ImageUpload->fileUpload('banner');
                            $data_imagem = $upload->getData();
                            if ($data_imagem) {
                                $options['imagem_id'] = $data_imagem['id'];
                            }
                        }
                    }
                    if (!isset($msg['error'])) {
                        if ($this->Site->edit($options, $id)) {
                            $msg['success'] = 'Site alterado com sucesso.';
                            $msg['redirect'] = site_url('/painel/sites');
                        } else {
                            $msg['error'] = 'Erro ao cadastrar seu site.';
                        }
                    }
                } else {
                    $msg['error'] = 'Você não tem permissão para alterar este site.';
                }
            }
            $this->outputJson($msg);
        } else {
            $this->set('site', $this->Site->getById($id));
            $this->display('sites/edit');
        }
    }
}

/* End of file Sites.php */
/* Location: ./application/modules/Sites/controllers/Sites.php */
