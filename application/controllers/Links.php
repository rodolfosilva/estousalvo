<?php defined('BASEPATH') or exit('No direct script access allowed');

class Links extends AppController
{
    public function __construct()
    {
        parent::__construct();
        if ($this->router->fetch_method() != 'redirect') {
            $this->checkIsLoged();
        }
        isset($this->Link) or $this->load->model('Link');
    }

    public function redirect($slug)
    {
        if ($result = $this->Link->getBySlug($slug)) {
            $default = array($slug => true);
            if ($data = $this->session->userdata('link_clikado')) {
                if (!isset($data[$slug])) {
                    $this->Link->addClick($result['id']);
                }
                $this->session->set_userdata('link_clikado', array_merge($data, $default));
            } else {
                $this->session->set_userdata('link_clikado', $default);
                $this->Link->addClick($result['id']);
            }
            // $this->title($result['titulo']);
            // $this->set('url', $result['url']);
            // $this->display('links/view');
            redirect($result['url']);
        } else {
            show_404();
            exit();
        }
    }

    public function index($offset = 1)
    {
        $this->load->library('pagination');
        $limit = 15;
        $this->pagination->initialize(
            $this->configPagination(
                'painel/links',
                $limit,
                $this->Link->totalLinksByUser($this->getCurUser('id')),
                3,
                3
            )
        );
        $this->set('paginacao', $this->pagination->create_links());
        $this->set('links', $this->Link->getLinksByUser($this->getCurUser('id'), $limit, ($offset - 1)*$limit));
        $this->display('links/index');
    }

    public function add()
    {
        $this->load->model('Categoria');
        $this->set('categorias', $this->arrayToList($this->Categoria->getAll(), 'id', 'nome', 'Selecione uma categoria'));
        $this->display('links/add');
    }

    public function edit($id = null)
    {
        if (!empty($id)) {
            $link = $this->Link->getById($id);
            if (isset($link['user_id']) && $link['user_id'] == $this->getCurUser('id') && $id > 0) {
                if ($this->input->is_ajax_request() && $this->input->post()) {
                    $msg = array();
                    try {
                        $current_date = date('Y-m-d H:i:s');
                        $titulo = trim($this->input->post('titulo'));
                        $categoria = trim($this->input->post('categoria'));
                        $descricao = trim($this->input->post('descricao'));
                        // $img = trim($this->input->post('image'));
                        // $img_local = isset($_FILES['image_post']) ? $_FILES['image_post'] : false;
                        $post_date = trim($this->input->post('post_date'));
                        $valida_data = !empty($post_date) && $link['post_date'] > $current_date ? true : false;
                        if (empty($titulo)) {
                            $msg['error'] = 'Digite o titulo da postagem.';
                        } elseif ($valida_data && !validaData($post_date, 'd/m/Y H:i')) {
                            $msg['error'] = 'Data invalida.';
                        } elseif (empty($categoria)) {
                            $msg['error'] = 'Selecione a categoria.';
                        } else {
                            $options = array(
                                'categoria_id' => $categoria,
                                'titulo' => htmlspecialchars($titulo, ENT_QUOTES),
                                'descricao' => htmlspecialchars($descricao, ENT_QUOTES),
                                'modified' => $current_date,
                            );
                            if ($valida_data) {
                                $options['post_date'] = converteData('d/m/Y H:i', 'Y-m-d H:i:s', $post_date);
                                $options['post_date'] = $options['post_date'] < $current_date ? $current_date : $options['post_date'];
                            }
                            if ($this->Link->edit($options, $id)) {
                                $msg['success'] = 'Link alterado com sucesso.';
                                // $msg['redirect'] = site_url('/painel/links');
                            } else {
                                $msg['error'] = 'Erro ao alterar o seu link.';
                            }
                        }
                    } catch (Exception $e) {
                        $msg['error'] = 'Erro ao tentar salvar este link.';
                    }

                    $this->outputJson($msg);
                } else {
                    $this->load->model('Categoria');
                    $this->set('id', $id);
                    $this->set('link', $link);
                    $this->set('categorias', $this->arrayToList($this->Categoria->getAll(), 'id', 'nome', 'Selecione uma categoria'));
                    $this->display('links/edit');
                }
            } else {
                show_404();
                exit();
            }
        } else {
            show_404();
            exit();
        }
    }

    public function addNewLink()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $msg = array();
            try {
                $url = trim($this->input->post('url'), '/');
                $titulo = trim($this->input->post('titulo'));
                $categoria = trim($this->input->post('categoria'));
                $descricao = trim($this->input->post('descricao'));
                $img = trim($this->input->post('image'));
                $img_local = isset($_FILES['image_post']) ? $_FILES['image_post'] : false;
                $post_date = trim($this->input->post('post_date'));

                if (empty($url)) {
                    $msg['error'] = 'Digite o link da postagem.';
                } elseif (!validar_url($url)) {
                    $msg['error'] = 'Link invalido.';
                } elseif (empty($titulo)) {
                    $msg['error'] = 'Digite o titulo da postagem.';
                } elseif (empty($categoria)) {
                    $msg['error'] = 'Selecione a categoria.';
                } elseif ($this->Link->getByUrl($url)) {
                    $msg['error'] = 'Este link ja esta cadastrado.';
                } elseif ($sites = $this->getCurUser('sites')) {
                    $domain_link = get_url_domain($url, true);
                    $data_site = false;
                    if (is_array($sites)) {
                        foreach ($sites as $site) {
                            if (get_url_domain($site['url'], true) == $domain_link) {
                                $data_site = $site;
                                break;
                            }
                        }
                    }
                    if (!$data_site) {
                        $msg['error'] = 'Você so pode enviar link dos seus sites.';
                    } elseif (isset($data_site['confirmacao']) && $data_site['confirmacao'] == 0) {
                        $msg['error'] = 'Você ainda não confirmou este domain.';
                    }
                } else {
                        $msg['error'] = 'Você aina não cadastrou nenhum site.';
                }
                if (!isset($msg['error'])) {
                    $data_imagem = array('id' => null);
                    isset($this->ImageUpload) or $this->load->library('ImageUpload', null, 'ImageUpload');

                    if (empty($img) && $img_local) {
                        // Verifica se o arquivo é uma imagem
                        if (!preg_match('/^image\/(pjpeg|jpeg|png|gif|bmp)$/', $img_local['type'])) {
                            $msg['error'] = 'O arquivo não é uma imagem.';
                        } elseif ($img_local['size'] > (2 * 1024 * 1024)) {
                            $msg['error'] = 'A imagem deve ter no máximo '.$tamanho.' bytes';
                        } elseif (!isset($msg['error'])) {
                            $upload = $this->ImageUpload->fileUpload('image_post');
                            $data_imagem = $upload->getData();
                        }
                    } elseif (!empty($img)) {
                        $upload = $this->ImageUpload->uploadURL($img);
                        $data_imagem = $upload->getData();
                    } else if (empty($descricao)) {
                        $msg['error'] = 'Não foi selecionado nenhuma imagem para seu link, digite pelo menos a descrição.';
                    }
                    if (is_array($data_imagem) && !empty($data_imagem['id'])) {
                        $x = trim($this->input->post('x'));
                        $y = trim($this->input->post('y'));
                        $h = trim($this->input->post('h'));
                        $w = trim($this->input->post('w'));
                        $upload->crop($data_imagem['id'], $x, $y, $w, $h, 240, 200);
                    } elseif (!empty($data_imagem['id'])) {
                        $msg['error'] = 'Erro ao enviar a imagem.';
                    }
                    if (!isset($msg['error'])) {
                        $current_date = date('Y-m-d H:i:s');
                        $new_post_date = $current_date;
                        if (!empty($post_date)) {
                            $tmp_post_date = explode('/', $post_date);
                            if (count($tmp_post_date) == 3) {
                                $tmp_data_hora = explode(' ', $tmp_post_date['2']);
                                $tmp_post_date = $tmp_data_hora[0].'-'.$tmp_post_date['1'].'-'.$tmp_post_date[0].' '.$tmp_data_hora[1].':00';
                                $new_post_date = $tmp_post_date > $new_post_date ? $tmp_post_date : $new_post_date;
                            }
                        }
                        $link_id = $this->Link->add(
                            array(
                                'categoria_id' => $categoria,
                                'imagem_id' => $data_imagem['id'],
                                'site_id' => $data_site['id'],
                                'titulo' => htmlspecialchars($titulo, ENT_QUOTES),
                                'descricao' => htmlspecialchars($descricao, ENT_QUOTES),
                                'url' => $url,
                                'slug' => $this->Link->getNewSlug(),
                                'post_date' => $new_post_date,
                                'created' => $current_date,
                                'modified' => $current_date,
                            )
                        );
                        $msg['success'] = 'Link cadastrado com sucesso...';
                        $msg['redirect'] = site_url('painel/links');
                    }
                }
            } catch (Exception $e) {
                $msg['error'] = 'Erro ao carregar as imagens deste link.';
            }
            $this->outputJson($msg);
        } else {
            show_404();
            exit();
        }
    }

    public function toogleVisualizacao($id)
    {
        $current_data = $this->Link->getById($id);
        $acao = $current_data['published'] == 1 ? 'liberar' : 'bloquear';
        if (isset($current_data['user_id']) && $current_data['user_id'] == $this->getCurUser('id') && $id > 0) {
            if ($this->db->query('UPDATE links SET published = IF(published=1, 2, 1) WHERE id = ? AND published >= 1', array($id))) {
                $msg['success'] = 'Visualização '.$acao.'.';
            } else {
                $msg['error'] = 'Erro ao '.$acao.' a visualização.';
            }
        } else {
            $msg['error'] = 'Você não tem permissão para '.$acao.' a visualização deste link.';
        }
        if ($this->input->is_ajax_request()) {
            $this->outputJson($msg);
        } else {
            redirect('/painel/links');
        }
    }

    public function doUpload()
    {
        $config['upload_path'] = FCPATH . 'public/uploads/'.date('Y/');
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1024';
        //$config['max_width']  = '1280';
        //$config['max_height']  = '1280';
        $config['encrypt_name'] = true;//Trocará o nome do arquivo para um HASH

        $index_html = '<html>'."\n".
        '<head>'."\n".
        "\t".'<title>403 Forbidden</title>'."\n".
        '</head>'."\n".
        '<body>'."\n\n".
        "\t".'<p>Directory access is forbidden.</p>'."\n\n".
        '</body>'."\n".
        '</html>';

        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0777);
            $fp = fopen($config['upload_path'].'index.html', 'wb');
            fwrite($fp, $index_html);
            fclose($fp);
        }

        $config['upload_path'] .= date('m/');
        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0777);
            $fp = fopen($config['upload_path'].'index.html', 'wb');
            fwrite($fp, $index_html);
            fclose($fp);
        }

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) {
            die( $this->upload->display_errors());
        } else {
            $this->template->set('upload_data', $this->upload->data());
            $this->template->load('layouts/admin');
        }
    }
}

/* End of file Links.php */
/* Location: ./application/modules/Links/controllers/Links.php */
