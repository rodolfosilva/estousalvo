<?php defined('BASEPATH') or exit('No direct script access allowed');

class Posts extends AppController
{
    public function __construct()
    {
        parent::__construct();
        $this->checkIsLoged();
    }

    public function index($offset = 1)
    {
        $this->load->library('pagination');
        $this->load->model('Post');
        $limit = 5;
        $this->pagination->initialize(
            $this->configPagination(
                'painel/novidades',
                $limit,
                $this->Post->getCount(),
                3,
                3
            )
        );
        $this->set('paginacao', $this->pagination->create_links());
        $this->set('posts', $this->Post->getAll($limit, ($offset - 1)*$limit));
        $this->display('posts/index');
    }
}

/* End of file posts.php */
/* Location: ./application/modules/Posts/controllers/posts.php */
