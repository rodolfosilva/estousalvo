<?php defined('BASEPATH') or exit('No direct script access allowed');

class Carregar extends AppController
{
    public function getNewLinks()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $id = trim($this->input->post('id'));
            $links = array();
            if (!empty($id)) {
                isset($this->Link) or $this->load->model('Link');
                $links = array('novos_links' => $this->Link->getNewLinks($id));
            }
            $this->outputJson($links);
        }
    }

    public function getUltimasVisitas()
    {
        isset($this->Visita) or $this->load->model('Visita');
        $this->outputJson($this->Visita->ultimasVisitas());
    }

    public function configJs()
    {
        $config = array(
            'site_url' => site_url(),
            'base_url' => base_url(),
        );
        $this->output->set_status_header('200', 'OK')
                     ->set_header('Content-type: text/javascript')
                     ->cache(24*60*60)
                     ->set_output(";window.site_config=site_config=".json_encode($config).";");
    }

    public function compileCss()
    {
        $this->load->driver('cache');
        $styles = $this->cache->file->get('styles.css');
        if (!$styles) {
            $this->load->driver('minify');
            $this->load->library('lessc');
            try {
                $styles = $this->lessc->compileFile(FCPATH . 'public/css/styles.less');
            } catch (Exception $e) {
                debug($e->getMessage());
            }
            $styles = $this->minify->css->min($styles);
            if (defined('ENVIRONMENT') && ENVIRONMENT != 'development') {
                $this->cache->file->save('styles.css', $styles, 365 * 24 * 3600);
            }
        }
        $this->output->set_status_header('200', 'OK')
                     ->set_header('Content-type: text/css')
                     ->set_content_type('text/css')
                     ->set_output($styles);
    }

    public function checkaAutoriaSite($domain = null)
    {
        if (!empty($domain) || ($this->input->is_ajax_request() && $this->input->post('domain'))) {
            $msg = array();
            try {
                $url = ($this->input->post('domain') ? $this->input->post('domain') : $domain);
                if (!validar_url('http://'.$url)) {
                    $msg['error'] = 'Domain invalido.';
                } else {
                    $imagens = array();
                    $html = curlGetContents('http://'.$url);
                    if (trim($html) != '') {
                        $dom = new DOMDocument();
                        libxml_use_internal_errors(true);
                        $dom->loadHTML($html);
                        libxml_clear_errors();
                        $dom->preserveWhiteSpace = false;
                        $images = $dom->getElementsByTagName('img');
                        $confirma = false;
                        if (is_array($images) || is_object($images)) {
                            isset($this->Site) or $this->load->model('Site');
                            $msg['error'] = 'Token não encontrado.';
                            foreach ($images as $image) {
                                if ($image->hasAttribute('src') && $image->hasAttribute('data-estousalvo-token')) {
                                    $token = $image->getAttribute('data-estousalvo-token');
                                    if ($site = $this->Site->getByToken($token)) {
                                        if ($site['id'] > 0 && $site['url'] == $url) {
                                            $confirma = true;
                                            $this->Site->confirmaToken($token);
                                            unset($msg['error']);
                                            if ($userdata = $this->getCurUser()) {
                                                $userdata['sites'] = $this->Site->getSitesByUser($this->getCurUser('id'));
                                                $this->session->set_userdata(array('user_logged' => $userdata));
                                            }
                                            $msg['success'] = 'Site confirmado com sucesso.';
                                            $msg['redirect'] = site_url('/painel/sites');
                                            break;
                                        }
                                    }
                                }
                            }
                        } else {
                            $msg['error'] = 'Tag de validação não encontada.';
                        }
                    } else {
                        $msg['error'] = 'Erro de comunicação com o servidor.';
                    }
                }
            } catch (Exception $e) {
                $msg['error'] = 'Erro ao verificar a autoria do link.';
            }
            $this->outputJson($msg);
        } else {
            show_404();
            exit();
        }
    }

    public function thumb ($id)
    {
        try {
            isset($this->Imagem) or $this->load->model('Imagem');
            $imagem = $this->Imagem->getByMd5($id);
            if (!$imagem) {
                throw new Exception('Image not found');
            } else {
                @header('Cache-Control: max-age=' . 1800 . ', public, must-revalidate');
                @header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 1800) . ' GMT');
                $options = json_decode(json_decode($imagem['options']), true);
                $x       = $options['crop']['240x200']['x'];
                $y       = $options['crop']['240x200']['y'];
                $t_w     = $options['crop']['240x200']['t_w'];
                $t_h     = $options['crop']['240x200']['t_h'];
                $w       = $options['crop']['240x200']['w'];
                $h       = $options['crop']['240x200']['h'];

                $this->load->library(
                    'image_lib',
                    array(
                        'source_image'   => FCPATH . $imagem['diretorio'] . $imagem['arquivo'],
                        'x_axis'         => $x,
                        'y_axis'         => $y,
                        'x_axis'         => $x,
                        'width'          => $t_w,
                        'height'         => $t_h,
                        'orig_width'     => $w,
                        'orig_height'    => $h,
                        'quality'        => 100,
                        'dynamic_output' => true,
                        'maintain_ratio' => false,
                    )
                );

                if (!$this->image_lib->jcrop($x, $y, $t_w, $t_h, $w, $h)) {
                    throw new Exception($this->image_lib->display_errors());
                }
            }
        } catch (Exception $e) {
            show_404();
        }
    }

    public function getImagesUrl()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $msg = array();
            try {
                $url = trim($this->input->post('url'), '/');
                if (!validar_url($url)) {
                    $msg['error'] = 'Url invalida.';
                } else {
                    $imagens = array();
                    $html = curlGetContents($url);
                    if (trim($html) != '') {
                        $dom = new DOMDocument();
                        libxml_use_internal_errors(true);
                        $dom->loadHTML($html);
                        libxml_clear_errors();
                        $dom->preserveWhiteSpace = false;
                        $images = $dom->getElementsByTagName('img');
                        if (is_array($images) || is_object($images)) {
                            foreach ($images as $image) {
                                if ($image->hasAttribute('src')) {
                                    $img = $image->getAttribute('src');
                                    if (!validar_url($url)) {
                                        $p_url = parse_url($url);
                                        $base_url = $p_url['host'];
                                        if (substr(trim($img), 0, 1) == '/') {
                                            $img = $base_url.(substr(trim($img), 0, 1) == '/' ? '' : '/').$img;
                                        } else {
                                            $img = $url.(substr(trim($img), 0, 1) == '/' ? '' : '/').$img;
                                        }
                                    }
                                    if (!find_array_by_key($img, $imagens, 'src')) {
                                        $imagens[] = array('src' => $img);
                                    }
                                }
                            }
                        }
                    }

                    if (!empty($imagens)) {
                        $msg['imagens'] = $imagens;
                    } else {
                        $msg['error'] = 'Nenhuma imagem encontrada neste link.';
                    }
                }
            } catch (Exception $e) {
                $msg['error'] = 'Erro ao carregar as imagens deste link.';
            }
            $this->outputJson($msg);
        } else {
            show_404();
            exit();
        }
    }
}

/* End of file Carregar.php */
/* Location: ./application/modules/Carregar/controllers/Carregar.php */
