<?php defined('BASEPATH') or exit('No direct script access allowed');

class Home extends AppController
{
    public function __construct()
    {
        parent::__construct();
        $this->twiggy->title()->append($this->config->item('site_description'));
    }

    private function checkCategoriaAdulto($categoria = null)
    {
        if (!empty($categoria) && !get_cookie('conteudo_adulto') && strtolower($categoria) == 'adulto') {
            redirect('conteudo_adulto');
        }
        $this->set('current_categoria', $categoria);
    }

    private function links($categoria, $offset, $tipo = 'cliques')
    {
        switch ($tipo) {
            case 'ultimos':
                $uri_segmento = 1;
                $uri_segmento_categoria = 3;
                $url_prefix = '';
                break;
            default:
                $uri_segmento = 1;
                $uri_segmento_categoria = 3;
                $url_prefix = '';
                break;
        }

        isset($this->Link) or $this->load->model('Link');
        if (!empty($categoria) && !is_numeric($categoria)) {
            isset($this->Categoria) or $this->load->model('Categoria');
            if (!$this->Categoria->getBySlug($categoria)) {
                show_404();
                exit();
            }
            $this->checkCategoriaAdulto($categoria);
            $uri_segmento = $uri_segmento_categoria;
        } elseif ($offset == 0 || $categoria >= $offset) {
            $offset = $categoria;
            $categoria = null;
        } else {
            $categoria = null;
        }
        $limit = 32;

        $this->load->library('pagination');
        $this->pagination->initialize(
            $this->configPagination(
                $url_prefix.($categoria == null ? '/' : '/categoria/'.$categoria.'/'),
                $limit,
                $this->Link->getCountByNovos($categoria),
                $uri_segmento,
                5
            )
        );

        switch ($tipo) {
            case 'ultimos':
                $this->set('links', $this->Link->getNovosByUltimosEnviados($categoria, $limit, ($offset - 1)*$limit));
                break;
            default:
                $this->set('links', $this->Link->getNovosByClicks($categoria, $limit, ($offset - 1)*$limit));
                break;
        }
        $this->set('paginacao', $this->pagination->create_links());
        $this->display('home/index');
    }

    public function index($categoria = null, $offset = 1)
    {
        $this->links($categoria, $offset, 'cliques');
    }

    public function buscar($buscar, $categoria = null, $page = 1)
    {
        $buscar = trim($buscar);
        if (!empty($buscar)) {

            $uri_segmento = 4;
            $uri_segmento_categoria = 3;
            $url_prefix = '/buscar/'.urlencode($buscar);
            $limit = 32;

            isset($this->Link) or $this->load->model('Link');
            if (!empty($categoria) && !is_numeric($categoria) && $categoria != 'all') {
                isset($this->Categoria) or $this->load->model('Categoria');
                if (!$this->Categoria->getBySlug($categoria)) {
                    show_404();
                    exit();
                }
                $this->checkCategoriaAdulto($categoria);
                $uri_segmento = $uri_segmento_categoria;
            } elseif (is_numeric($categoria) && ($page == 0 || $categoria >= $page)) {
                $page = $categoria;
                $categoria = null;
            } else {
                $categoria = null;
            }

            $this->load->library('pagination');
            $this->pagination->initialize(
                $this->configPagination(
                    $url_prefix.($categoria == null ? '/all/' : '/'.$categoria.'/'),
                    $limit,
                    $this->Link->totalByBusca($buscar, $categoria),
                    $uri_segmento,
                    5
                )
            );

            $this->set('links', $this->Link->getByBusca($buscar, $categoria, $limit, $page));
            $this->set('paginacao', $this->pagination->create_links());
            $this->set('_buscar', $buscar);
            $this->set('_buscar_categoria', $categoria);
            $this->load->model('Categoria');
            $this->set('categorias', $this->arrayToList($this->Categoria->getAll(), 'slug', 'nome', ''));
            $this->display('home/buscar');

        } else {
            show_404();
            exit();
        }
    }

    public function ultimosEnviados($categoria = null, $offset = 1)
    {
        $this->links($categoria, $offset, 'ultimos');
    }

    public function conteudoAdulto($confirma = null)
    {
        if (trim($confirma) != '') {
            set_cookie(
                array(
                    'name' => 'conteudo_adulto',
                    'value' => true,
                    'expire' => 31104000// 360 dias
                )
            );
            redirect('categoria/adulto');
        }
        $this->display('home/conteudo_adulto');
    }

    public function view($slug = null)
    {
        if ($result = $this->Link->getBySlug($slug)) {
            $this->set('link', $result);
            $this->display('home/view');
        } else {
            show_404();
            exit();
        }
    }
}

/* End of file Home.php */
/* Location: ./application/modules/Home/controllers/Home.php */
