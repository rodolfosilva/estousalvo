<?php defined('BASEPATH') or exit('No direct script access allowed');

class Page extends AppController
{
    public function index()
    {
        $this->output->set_status_header('404');
        $this->view('error_404');
    }

    public function view($page = null)
    {
        $twiggy = $this->config->item('twiggy');
        is_file(APPPATH.$twiggy['themes_base_dir'].'/'.$twiggy['default_theme'].'/pages/'.$page.$twiggy['template_file_ext']) or show_404();
        $this->display('pages/'.$page);
    }

    public function contato()
    {
        if ($this->input->post()) {
            $nome          = htmlspecialchars(trim($this->input->post('nome')));
            $assunto          = htmlspecialchars(trim($this->input->post('assunto')));
            $email            = trim($this->input->post('email'));
            $mensagem         = htmlspecialchars(trim($this->input->post('mensagem')));
            $msg = array();
            if (empty($nome)) {
                $msg['error'] = 'Digite o seu nome.';
            } elseif (empty($assunto)) {
                $msg['error'] = 'Digite o assunto.';
            } elseif (strlen($assunto) > 200) {
                $msg['error'] = 'Assunto muito grande.';
            } elseif (empty($mensagem)) {
                $msg['error'] = 'Digite a sua mensagem.';
            } elseif (!$this->getCurUser() && empty($email)) {
                $msg['error'] = 'Digite seu email.';
            } elseif (!$this->getCurUser() && !preg_match('/^[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\-]+\.[a-z]{2,4}$/', $email)) {
                $msg['error'] = 'E-mail inválido, digite seu endereço de email no formato nome@example.com.';
            } else {
                $email = empty($email) ? $this->getCurUser() : $email;
                if ($this->enviarEmail(
                    'contato@rodolfosilva.com',
                    'Contato/Sugestões',
                    array(
                        array('destaque' => $nome),
                        array('destaque' => $assunto),
                        array('destaque' => $email),
                        array('text' => $mensagem),
                    ),
                    'Contato: '.$assunto
                )) {
                        $msg['success'] = 'Email enviado com successo.';
                } else {
                    $msg['error'] = 'Erro ao tentar enviar o email.';
                }
            }
            $this->outputJson($msg);
        } else {
            $this->display('pages/contato');
        }
    }

    public function rss()
    {
        $this->load->library(
            'RSSFeed',
            array(
                'title' => $this->config->item('site_name'),
                'link' => site_url('/'),
                'description' => $this->config->item('site_description'),
                'rsslink' => site_url('/rss'),
                'language' => 'pt-BR',
                'copyright' => 'Copyright (C) 2013 estousalvo.com',
            ),
            'RSSFeed'
        );

        $this->RSSFeed->setImage(base_url('public/img/logo.png'), $this->config->item('site_name'), site_url('/'));

        isset($this->Link) or $this->load->model('Link');
        $links = $this->Link->getNovosByClicks(null, 15);
        foreach ($links as $link) {
            $name = isset($link['image_arquivo']) && !empty($link['image_arquivo']) ? explode('.', $link['image_arquivo']) : array();
            if (isset($link['image_diretorio'], $name[0], $name[1])) {
                $imagem = $link['image_diretorio'].$name[0].'-240x200.'.$name[1];
            } else {
                $imagem = 'public/img/default-240x200.jpg';
            }
            $this->RSSFeed->addItem(
                $link['titulo'],
                site_url('go/'.$link['slug']),
                img(array('src' => $imagem, 'style' => 'float:left;')).$link['descricao'],
                $link['categoria_nome'],
                null,
                site_url('go/'.$link['slug']),
                strtotime($link['post_date'])
            );
        }

        $this->RSSFeed->displayXML();
    }
}

/* End of file Page.php */
/* Location: ./application/modules/Page/controllers/Page.php */
