<?php defined('BASEPATH') or exit('No direct script access allowed');

class Rankings extends AppController
{

    public function index($offset = 1)
    {
        isset($this->Ranking) or $this->load->model('Ranking');
        $this->load->library('pagination');
        $limit = 25;
        $offset = $offset >= 1 ? $offset : 1;
        $this->pagination->initialize(
            $this->configPagination(
                'rankings/index',
                $limit,
                $this->Ranking->totalRanking(),
                3,
                3
            )
        );
        $this->set('paginacao', $this->pagination->create_links());
        $this->set('rankings', $this->Ranking->getAll($limit, ($offset - 1) * $limit));
        $this->display('rankings/index');
    }
}

/* End of file Rankings.php */
/* Location: ./application/modules/Rankings/controllers/Rankings.php */
