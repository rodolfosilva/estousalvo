<h2 class="page_title">Novidades</h2>
<section id="layPageNovidades">
    <?php
    if (isset($posts) && is_array($posts)) {
        foreach ($posts as $post) {
    ?>
    <article>
        <header>
            <h1><?php echo $post['titulo'] ?></h1>
            <p>Postado em <time><?php echo date('d/m/Y H:m', strtotime($post['created'])); ?></time></p>
        </header>
        <p><?php echo $post['artigo'] ?></p>
    </article>
    <?php
        }
    }
    ?>
</section>
<div class="clearfix"></div>
<?php
echo isset($paginacao) && !empty($paginacao) ? '<div class="row" style="text-align:center;">'.$paginacao.'</div>': '';
