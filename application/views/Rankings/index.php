<h1 class="page_title">Rankings</h1>
<p class="text-info">* A lista de rankings é atualizada a cada 30 minutos automaticamente.</p>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th align="center" width="35">Ranking</th>
            <th width="230"></th>
            <th>Dados</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (is_array($rankings)) {
            foreach ($rankings as $ranking) {
                ?>
                <tr>
                    <td align="center" style="vertical-align:middle"><?php echo $ranking['ranking']; ?></td>
                    <td align="center" style="vertical-align:middle"><?php echo anchor('http://'.$ranking['url'], img(array('src' => $ranking['site_img'], 'height' => 40, 'width' => 230))); ?></td>
                    <td style="vertical-align:middle">
                        <strong>Titulo:</strong>&nbsp;<?php echo $ranking['titulo']?><br>
                        <strong>Endereço:</strong>&nbsp;<?php echo anchor('http://'.$ranking['url'], $ranking['url'], 'target="_blank"'); ?><br>
                        <strong>Links enviados:</strong>&nbsp;<?php echo ceil($ranking['links'])?><br>
                        <strong>Cliques recebidos:</strong>&nbsp;<?php echo ceil($ranking['cliques'])?><br>
                        <strong>Media de cliques por link:</strong>&nbsp;<?php echo (int)@ceil($ranking['cliques']/$ranking['links'])?><br>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>

<?php
echo $paginacao;
