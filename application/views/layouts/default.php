<?php defined('BASEPATH') or exit('No direct script access allowed');
echo doctype('html5'). "\n";
?>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="index,follow">
        <meta name="revisit-after" content="1 days">
        <meta name="language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="google-site-verification" content="Kg1pfgXqW8SjjeFpnrGfTHcEtXL-g-fq_x2eht3nZJc">
        <meta name="author" content="<?php echo $this->template->fetch('title'); ?>">
        <meta name="generator" content="<?php echo $this->template->fetch('title'); ?>">
        <meta name="keywords" content="">
        <meta name="description" content="<?php echo $this->template->fetch('desc'); ?>">
        <?php
        echo '<title>'.$this->template->fetch('title') . ($this->template->fetch('desc') !== '' ? ' - ' . $this->template->fetch('desc') : '' ).'</title>';

        $this->template->link('rss', array('rel' => 'alternate', 'type' => 'application/rss+xml', 'title' => $this->template->fetch('title') . ' - RSS'), false);
        $this->template->link('favicon.ico', array('rel' => 'icon', 'type' => 'image/x-ico'), false);
        $this->template->link('favicon.ico', array('rel' => 'shortcut icon', 'type' => 'image/x-ico'), false);

        $this->template->css('public/css/styles.css', null, false);

        $this->template->script('public/js/jquery.min.js', null, false, -999);
        $this->template->script('public/js/bootstrap.min.js', null, false, -998);
        $this->template->script('public/js/jquery.form.min.js', null, false, -997);
        $this->template->script('public/js/jquery.mousewheel.js', null, false, -996);
        $this->template->script('public/js/jquery.jscrollpane.min.js', null, false, -995);

        $this->template->script('load/config.js', null, false, 996);
        $this->template->script('public/js/app/feedback.js', null, false, 997);
        $this->template->script('public/js/app/estousalvo.js', null, false, 998);
        $this->template->script('public/js/scripts.js', null, false, 999);

        $this->template->scriptBlockBefore('var jQ=jQuery.noConflict();jQ(document).ready(function($){'."\n");
        $this->template->scriptBlockAfter('});'."\n");
        echo $this->template->fetch('links');
        echo $this->template->fetch('scripts');
        echo $this->template->fetch('styles');
        echo $this->template->fetch('script_block');
        echo $this->template->fetch('style_block');
        ?>
        <!--[if lt IE 9]>
        <?php
        // Adiciona suporte a HTML5 para IE6-8
        echo $this->template->script('public/js/html5shiv.js');
        ?>
        <![endif]-->
    </head>

    <body>
    <div id="wrap">
        <div id="cabecalho">
            <div class="container">
                <div class="row">
                    <?php echo anchor('/', img(array('src' => 'public/img/logo-lg.png', 'class'=>'img-responsive')), 'class="logo"'); ?>
                </div>
            </div>
        </div>
        <header id="menu-topo" class="navbar navbar-default navbar-menu-topo">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?php echo anchor('/', 'EstouSalvo', 'id="logo" class="navbar-brand"'); ?>
                </div>
                <nav class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <?php
                        echo '<li>'.anchor('/', 'Página inicial').'</li>';
                        echo '<li>'.anchor('http://blog.estousalvo.com.br', 'Blog', 'target="_blank"').'</li>';
                        echo '<li>'.anchor('/rankings', 'Rankings').'</li>';
                        if (isset($menu_categorias) && is_array($menu_categorias) && !empty($menu_categorias)) {
                            echo '<li>'.anchor('/', 'Categorias <b class="caret"></b>', ' class="dropdown-toggle" data-toggle="dropdown"');
                                echo '<ul class="dropdown-menu">';
                                foreach ($menu_categorias as $value) {
                                    echo '<li>'.anchor('/categoria/'.$value['slug'], $value['nome']).'</li>';
                                }
                                echo '</ul>';
                            echo '</li>';
                        }
                        ?>
                        <!-- <li>
                            <form class="navbar-form">
                                <input type="text" class="form-control" placeholder="Search">
                                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </form>
                        </li> -->
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <?php
                        if (!isset($_CURUSER['nome'])) {
                            echo '<li>'.anchor('/painel/add_link', 'Enviar link').'</li>';
                            echo '<li>'.anchor('/registro', 'Cadastro').'</li>';
                            echo '<li>'.anchor('/login', 'Entrar').'</li>';
                        } else {
                            echo '<li>'.anchor('painel/links', '<span class="fa fa-user"></span>&nbsp;Painel de controle <b class="caret"></b>', ' class="dropdown-toggle" data-toggle="dropdown"');
                                echo '<ul class="dropdown-menu">';
                                    echo '<li>'.anchor('/painel/add_link', '<span class="fa fa-link"></span>&nbsp;Enviar link').'</li>';
                                    echo '<li>'.anchor('/painel/sites', '<span class="fa fa-globe"></span>&nbsp;Meus sites').'</li>';
                                    echo '<li>'.anchor('/painel/links', '<span class="fa fa-cloud"></span>&nbsp;Meus links').'</li>';
                                    echo '<li class="divider"></li>';
                                    echo '<li>'.anchor('/painel/configuracoesDaConta', '<span class="fa fa-cog"></span>&nbsp;Configurações da conta').'</li>';
                                    echo '<li>'.anchor('/painel/password', '<span class="fa fa-key"></span>&nbsp;Mudar minha senha').'</li>';
                                    echo '<li class="divider"></li>';
                                    echo '<li>'.anchor('/logout', '<span class="fa fa-sign-out"></span>&nbsp;Sair').'</li>';
                                echo '</ul>';
                            echo '</li>';
                        }
                        echo '<li class="adulto">'.anchor('/categoria/adulto', '+18').'</li>';
                        ?>
                    </ul>
                </nav>
            </div>
        </header><!-- end topo -->

        <div id="mensagem">
            <div class="container">
                <div></div>
            </div>
        </div><!-- end mensagem -->

        <div id="header" class="visible-md visible-lg">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 visible-md visible-lg">
                        <div class="row" id="parceiros">
                            <?php
                            for ($i = 0; $i < 9; $i++) {
                                $parceiro_top = array(
                                    'domain' => site_url('/'),
                                    'total_visitas' => 'Seja parceiro cadastre-se',
                                    'titulo' => 'Seja parceiro cadastre-se no Estou Salvo',
                                    'target' => '_self',
                                    'imagem' => 'public/img/default-230x40.jpg',
                                );
                                if (isset($ultimas_visitas[$i])) {
                                    $parceiro_top = array_merge($parceiro_top, $ultimas_visitas[$i]);
                                    $parceiro_top['total_visitas'] = sprintf('Enviou %d visitas na ultima hora!', $parceiro_top['total_visitas']);
                                    $parceiro_top['domain'] = 'http://'.$parceiro_top['domain'];
                                    $parceiro_top['target'] = '_blank';
                                    $parceiro_top['imagem'] = $ultimas_visitas[$i]['site_img'];
                                }
                                $parceiro_img = img(
                                    array(
                                        'src' => $parceiro_top['imagem'],
                                        'alt' => $parceiro_top['titulo'],
                                        'width' => '230',
                                        'height' => '40',
                                        'title' => $parceiro_top['titulo'],
                                    )
                                );
                            ?>
                            <div class="col-md-4">
                                    <?php echo anchor($parceiro_top['domain'], $parceiro_img.'<span>'.$parceiro_top['total_visitas'].'</span>', sprintf('class="parceiro" target="%s" title="%s"', $parceiro_top['target'], $parceiro_top['titulo'])); ?>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3 visible-md visible-lg">
                        <div class="scrollable" id="timeline">
                            <?php
                            foreach ($novos_links['novos_links'] as $novo_link) {
                            ?>
                            <div class="item" data-id="<?php echo $novo_link['id']; ?>">
                                <div class="top">
                                    <img src="http://www.google.com/s2/favicons?domain=<?php echo $novo_link['site_url']; ?>" height="16" width="16" />
                                    <?php echo $novo_link['site']; ?>
                                </div>
                                <a class="title" target="_blank" href="<?php echo $novo_link['permanlink']; ?>"><?php echo $novo_link['titulo']; ?></a>
                                <div class="footer">
                                    <a href="<?php echo $novo_link['categoria_link']; ?>" class="category"><?php echo $novo_link['categoria']; ?></a>
                                    <time class="date"><?php echo date('d/m/Y H:i:s', strtotime($novo_link['data'])); ?></time>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end header -->

        <div id="middle" class="container">
            <?php echo $this->template->fetch('contents'); ?>
        </div><!-- end middle -->
    </div>

    <footer id="footer">
        <div class="container">
            <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Ffan.estousalvo&amp;width&amp;height=290&amp;width=1100&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=false&amp;appId=604030859657520" class="col-xs-12 likebox" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
        </div>
    </footer>
    <footer id="credit">
        <div class="container">
            <?php echo anchor('/', 'EstouSalvo.com.br', ' class="logo"'); ?>
            <p class="muted text-left">&copy; EstouSalvo 2013<?php echo(date('Y') > 2013 ? '&nbsp;-&nbsp;'.date('Y') : ''); ?>&nbsp;-&nbsp;Todos os direitos reservados. <?php echo anchor('page/politica_privacidade', 'Política de privacidade'); ?></p>
        </div>
    </footer>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-37550006-2', 'estousalvo.com');
        ga('send', 'pageview');
    </script>
</body>
</html>
