<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tbody>
        <tr>
            <td bgcolor="#FBFBFB" align="center">
                <table width="640" cellpadding="0" cellspacing="0" border="0" style="margin:10px">
                    <tbody>
                        <tr>
                            <td bgcolor="#fff" style="font-family:Helvetica,Arial,Geneva,sans-serif;border-radius:0 0 10px 10px">
                            <table cellpadding="0" cellspacing="0" border="0"
                                style="border-radius:10px">
                                    <tbody>
                                        <tr>
                                            <td><a style="background:#1F8FB0;padding:20px;display:block;outline:none;text-align:left;" href="<?php echo site_url('/'); ?>"><img alt="EstouSalvo" src="<?php echo base_url('/public/img/logo.gif'); ?>" /></a></td>
                                        </tr>

                                        <tr>
                                            <td style="padding:20px">
                                                <table cellpadding="0" cellspacing=
                                                "0" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="font-family:Helvetica,Arial,Geneva,sans-serif;font-size:18px;color:#444;font-weight:bold;line-height:24px;padding-top:10px"><?php echo $cumprimento; ?></td>
                                                            <td style="text-align:right;font-family:Helvetica,Arial,Geneva,sans-serif;font-size:16px;color:#888;font-weight:bold;line-height:24px;padding-top:10px"><?php echo $assunto ?></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2">
                                                                <?php
                                                                    foreach ($mensagem as $key => $value) {
                                                                        if (isset($value['text'])) {
                                                                            echo '<p style="font-family:Helvetica,Arial,Geneva,sans-serif;font-size:13px;color:#333;line-height:18px;padding:5px 0;margin:0;">'.$value['text'].'</p>';
                                                                        } elseif (isset($value['link'])) {
                                                                            echo '<p style="font-family:Helvetica,Arial,Geneva,sans-serif;font-size:13px;color:#333;line-height:18px;padding:5px 0;margin:0;color:#6d400a;background:#ffecbe;border-radius:3px;text-align:center;font-weight:bold"><a style="color:#0088cc;text-decoration:none" href="'.$value['link'].'" target="_blank">'.$value['link'].'</a></p>';
                                                                        } elseif (isset($value['destaque'])) {
                                                                            echo '<p style="font-family:Helvetica,Arial,Geneva,sans-serif;font-size:13px;color:#333;line-height:18px;padding:5px 0;margin:0;color:#6d400a;background:#ffecbe;border-radius:3px;text-align:center;font-weight:bold">'.$value['destaque'].'</a></p>';
                                                                        }
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family:Helvetica,Arial,Geneva,sans-serif;font-size:11px;padding:10px 2px 0 2px;color:#666">&copy; EstouSalvo.com.br 2013</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
