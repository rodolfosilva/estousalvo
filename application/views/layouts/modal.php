<?php defined('BASEPATH') OR exit('No direct script access allowed');
echo $this->template->scriptBlockBefore('var jQ=jQuery.noConflict();jQ(document).ready(function($){'."\n");
echo $this->template->scriptBlockAfter('});'."\n");
echo $this->template->fetch('links');
echo $this->template->fetch('scripts');
echo $this->template->fetch('styles');
echo $this->template->fetch('contents');
