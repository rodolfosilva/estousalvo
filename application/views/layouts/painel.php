<div>
    <div class="col-lg-3">
        <div style="background: url(<?php echo getGravatar($_CURUSER['email'], 180, urlencode('http://placehold.it/180x160')); ?>) no-repeat center center;;width:180px;height:180px;margin:0 auto 15px;"></div>
        <ul class="nav nav-pills nav-stacked">
            <li<?php echo $painel_tab == 'posts/index' ? ' class="active"' : ''; ?>><?php echo anchor('painel/novidades', 'Novidades'); ?><!-- <span class="badge pull-right">42</span> --></li>
            <li<?php echo $painel_tab == 'links/index' ? ' class="active"' : ''; ?>><?php echo anchor('painel/links', 'Meus links'); ?><!-- <span class="badge pull-right">42</span> --></li>
            <li<?php echo $painel_tab == 'links/add' ? ' class="active"' : ''; ?>><?php echo anchor('painel/addLink', 'Enviar novo link'); ?></li>
            <li<?php echo $painel_tab == 'sites/index' || $painel_tab == 'sites/ativar' ? ' class="active"' : ''; ?>><?php echo anchor('painel/sites', 'Meus sites'); ?><!-- <span class="badge pull-right">42</span> --></li>
            <li<?php echo $painel_tab == 'sites/add' ? ' class="active"' : ''; ?>><?php echo anchor('painel/addSite', 'Adicionar site'); ?></li>
            <li<?php echo $painel_tab == 'users/edit' ? ' class="active"' : ''; ?>><?php echo anchor('painel/configuracoesDaConta', 'Configurações da conta'); ?></li>
            <li<?php echo $painel_tab == 'users/password' ? ' class="active"' : ''; ?>><?php echo anchor('painel/password', 'Mudar senha'); ?></li>
        </ul>
    </div>
    <div class="col-lg-9">
        <?php echo isset($painel_content) ? $painel_content : ''; ?>
    </div>
</div>
