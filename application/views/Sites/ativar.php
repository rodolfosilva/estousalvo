<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<h2 class="page_title">Ativar seu site<span class="btn btn-primary pull-right confirmarSite" data-domain="<?php echo $site['url']?>">Ja adicionou o banner? Clique aqui!</span></h2>
<p>Para ativar o seu site, basta colocar um dos nossos banners em seu site. Os banners abaixo foram feitos com um código exclusivo para a validação do seu site. Caso você tenha desenvolvido um banner personalizado adicione este atributo a sua imagem <code>data-estousalvo-token="<?php echo $site['token']; ?>"</code></p>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th width="50">Tamanho</th>
            <th width="468">Banner</th>
            <th>Código</th>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach($banners as $banner) {
                list($banner['width'], $banner['height']) = explode('x', strtolower($banner['size']));
                $img = img(array('width' => $banner['width'], 'height' => $banner['height'], 'src' => $banner['url'], 'title' => $banner['descricao'], 'alt' => $banner['descricao'], 'data-estousalvo-token' => $site['token']));
        ?>
        <tr>
            <td align="center" style="vertical-align:middle"><?php echo $banner['size']; ?></td>
            <td align="center" style="vertical-align:middle"><?php echo $img; ?></td>
            <td align="center" style="vertical-align:middle">
                <textarea class="form-control" rows="3 "><?php echo htmlspecialchars(anchor('/', $img, array('target' => '_blank', 'title' => $banner['descricao']))); ?></textarea>
            </td>
        </tr>
        <?php
            }
        ?>
    </tbody>
</table>
