<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<h2 class="page_title">Adicionar novo site</h2>

<?php echo form_open('painel/add_site', array('id' => 'layCadastroSite','class' => 'form-horizontal ajaxSubmit', 'accept-charset' => 'utf-8', 'role' => 'form')); ?>
    <div class="form-group">
        <label class="col-lg-3 control-label">Titulo</label>
        <div class="col-lg-9">
            <input type="text" class="form-control" name="titulo" placeholder="Titulo do seu blog ex.: EstouSalvo">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label">Endereço</label>
        <div class="col-lg-9">
            <input type="text" class="form-control" name="url" placeholder="Endereço do seu site sem www ex.: estousalvo.com.br">
        </div>
    </div>
    <button type="submit" form="layCadastroSite" class="btn btn-labeled btn-success pull-right">
        <span class="btn-label"><i class="fa fa-check"></i></span>Salvar
    </button>
<?php
echo form_close();
