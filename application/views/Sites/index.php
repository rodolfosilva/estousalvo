<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<h2 class="page_title">Meus sites<?php echo anchor('painel/add_site', '<span class="btn-label"><i class="fa fa-plus"></i></span> Adicionar novo site', 'class="btn btn-info btn-labeled pull-right"'); ?></h2>

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th width="230">#</th>
            <th>Dados</th>
            <th width="100">Status</th>
            <th width="130"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (is_array($sites)) {
            foreach ($sites as $site) {
                ?>
                <tr>
                    <td align="center" style="vertical-align:middle"><?php echo img(array('src' => $site['site_img'], 'width' => '230', 'height' => '40')); ?></td>
                    <td style="vertical-align:middle">
                        <strong>Titulo:</strong>&nbsp;<?php echo $site['titulo']?><br>
                        <strong>Token:</strong>&nbsp;<?php echo $site['token']?><br>
                        <strong>Domain:</strong>&nbsp;<?php echo anchor('http://'.$site['url'], $site['url'], 'target="_blank"'); ?><br>
                    </td>
                    <td align="center" style="vertical-align:middle">
                        <?php
                        switch ($site['confirmacao']) {
                            case '0':
                                echo anchor('painel/ativarSite/'.$site['token'], 'Confirmar agora?', 'class="label label-warning"');
                                break;
                            case '1':
                            ?>
                            <span class="label label-success">Confirmado</span>
                            <?php
                                break;
                            case '2':
                            ?>
                            <span class="label label-important">Bloqueado</span>
                            <?php
                                break;
                        }
                        ?>
                    </td>
                    <td style="vertical-align:middle;text-align:center;">
                        <a href="<?php echo site_url('painel/edit_site/'.$site['id']); ?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Editar site"><span class="glyphicon glyphicon-cog"></span></a>
                        <?php if ($site['published'] == 1) { ?>
                            <a href="<?php echo site_url('painel/toggle_site/'.$site['id']); ?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Bloquear visualização"><span style="color:#D9534F" class="glyphicon glyphicon-eye-close"></span></a>
                        <?php } else { ?>
                            <a href="<?php echo site_url('painel/toggle_site/'.$site['id']); ?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Desbloquear visualização"><span style="color:#5CB85C" class="glyphicon glyphicon-eye-open"></span></a>
                        <?php } ?>
                            <a href="<?php echo site_url('painel/bannerSite/'.$site['token']); ?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Banners para este site"><span style="color:#5CB85C" class="fa fa-picture-o"></span></a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
