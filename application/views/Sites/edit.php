<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<h2 class="page_title">Alterar dados do site</h2>

<?php echo form_open_multipart('painel/edit_site/'.$site['id'], array('id' => 'layEditaSite','class' => 'form-horizontal ajaxSubmit', 'accept-charset' => 'utf-8', 'role' => 'form')); ?>
    <div class="form-group">
        <label class="col-lg-3 control-label">Titulo</label>
        <div class="col-lg-9">
            <input type="text" class="form-control" value="<?php echo $site['titulo']; ?>" name="titulo" placeholder="Titulo do seu blog ex.: EstouSalvo">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label">Endereço</label>
        <div class="col-lg-9">
            <input type="text" class="form-control" value="<?php echo $site['url']; ?>" name="url" placeholder="Endereço do seu site sem www ex.: estousalvo.com.br">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label">Banner</label>
        <div class="col-lg-9">
            <?php echo img($site['site_img']); ?>
            <span class="btn btn-primary btn-labeled btn-file">
                <span class="btn-label"><i class="fa fa-check"></i></span>Mudar banner
                <input type="file" name="banner" />
            </span>
            <div>
                <small>Dimensões recomendadas de 230 x 40. Tamanho máximo do arquivo é de 700KB</small>
            </div>
        </div>
    </div>
    <input type="hidden" value="<?php echo $site['id']; ?>" name="id">
    <button type="submit" form="layEditaSite" class="btn btn-labeled btn-success pull-right">
        <span class="btn-label"><i class="fa fa-check"></i></span>Salvar
    </button>
<?php
echo form_close();
