<center>
    <h1>Server Error: 404 (Not Found)</h1>
    <?php echo img('public/img/404.jpg') ?>
    <h2>O que isso significa?</h2>
    <p>Não foi possível encontrar a página que você solicitou em nossos servidores. Sentimos muito sobre isso. A culpa é nossa, não sua.<br />
Talvez você queira ir para a <?php echo anchor('/', 'página inicial'); ?>?</p>
</center>
