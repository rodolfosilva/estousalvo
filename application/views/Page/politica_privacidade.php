<h1 class="page_title">Política de privacidade</h1>

<p>Reconhecemos a extrema importância da privacidade dos usuários e visitantes do <?php echo anchor('/', '<strong>EstouSalvo</strong>'); ?>. Pensando nisso deixamos-lhes o tipo de informação pessoal que recebemos e coletamos quando você visita ou se cadastra em nosso sistema. Estas informações nunca serão vendidas ou distribuídas para terceiros.</p>

<h2>Os registros:</h2>
<p>O endereço de Email utilizado durante o cadastramento é utilizado apenas pelo site para enviar comunicados e responder requisições. Através deste Email é feita a validação do cadastro do usuário, e por ele sera possível efetuar a redefinição da senha de acesso ao sistema, caso a mesma tenha sido perdida.</p>
<p>O endereço IP coletado pelo nosso sistema é utilizado apenas para gerar relatórios estatísticos de visitas, referências de acesso e tempo de duração da sua visita.</p>
<p>Em hipótese alguma estes emails e IP's são ou serão repassados para terceiros.</p>

<h2>Os cookies:</h2>

<p>Os cookies são utilizados apenas para guardar informações referentes às suas preferências em nosso sistema. Através destes cookies também é feito o controle de acesso a links e contador de visitas, sendo gravado e contabilizado o tempo de duração no <?php echo anchor('/', '<strong>EstouSalvo</strong>'); ?>.</p>

<p>Você pode desligar os cookies, nas configurações do seu navegador(browser), ou através do seu Anti-Vírus. Porem isso poderá impactar em algumas funcionalidades do site que utilizam os cookies para guardar suas preferências.</p>

<h2>Link de terceiros:</h2>

<p>O <?php echo anchor('/', '<strong>EstouSalvo</strong>'); ?> está repleto de links para outros sites. Não é de nossa responsabilidade a política de privacidade implantada nos sites de terceiros.</p>

<h2>Exceções específicas:</h2>
<ul>
    <li>Ordens judiciais têm o poder de sobrepor a política de privacidade aqui apresentada.</li>
    <li>Em casos onde o conteúdo do link envolva crime de pedofilia, todas as suas informações serão voluntariamente repassadas para às autoridades competentes.</li>
</ul>
<h2>Segurança:</h2>

<p>Para garantir a segurança dos dados de nossos usuários e visitantes, utilizamos o que há de novo em tecnologia de servidores e uma equipe para cuidar da codificação e documentação do sistema. Tentamos ao máximo lacrar o <em><u>core</u></em> da nossa aplicação para evitar possíveis falhas. É feito diariamente o Backup das informações em geral de todo o conteúdo do site.</p>

<h2>Sites:</h2>

<p>Os sites cadastrados no <?php echo anchor('/', '<strong>EstouSalvo</strong>'); ?> só estarão disponíveis após o usuário efetivar a confirmação da autoria do site adicionando um dos nossos banners. Após confirmados, estes sites estarão liberados para a publicação de novos links. Este processo de ativação é efetuado pelo próprio usuário através do painel de controle.</p>
