<?php
defined('BASEPATH') or exit('No direct script access allowed');
echo $this->template->css('public/css/jquery.Jcrop.min.css', null, false);
echo $this->template->css('public/css/bootstrap-datetimepicker.min.css', null, false);
echo $this->template->script('public/js/jquery.Jcrop.min.js', null, false, true);
echo $this->template->script('public/js/moment.min.js', null, false, true);
echo $this->template->script('public/js/bootstrap-datetimepicker.min.js', null, false, true);
echo $this->template->script('public/js/locales/bootstrap-datetimepicker.pt-BR.js', null, false, true);
?>
<h2 class="page_title">Enviar novo link</h2>
<?php echo form_open('links/addNewLink', array('id' => 'layCadastroLink', 'name' => 'layCadastroLink','class' => 'form-horizontal ajaxSubmit', 'accept-charset' => 'utf-8', 'role' => 'form')); ?>
    <div class="passo-1">
        <div class="form-group">
            <label for="inpLink" class="col-lg-3 control-label">Link do seu post</label>
            <div class="col-lg-9">
                <?php echo form_input(array('name' => 'url', 'id' => 'inpLink', 'class' => 'form-control', 'placeholder' => 'http://domain.com/posts/minha-postagem')); ?>
                <span class="help-inline">Informe o link completo para o conteúdo do blog que deseja sugerir, da forma exata como ele é exibido na barra de endereços do seu navegador.</span>
            </div>
        </div>
        <div class="form-group">
            <label for="inpTitulo" class="col-lg-3 control-label">Publicar</label>
            <div class="col-lg-9 input-group date form_datetime" id="inpData">
                <?php echo form_input(array('name' => 'post_date', 'class' => 'form-control', 'placeholder' => 'Imediatamente', 'readonly' => null)); ?>
                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
            </div>
        </div>
        <div class="form-group">
            <label for="inpTitulo" class="col-lg-3 control-label">Título</label>
            <div class="col-lg-9">
                <?php echo form_input(array('name' => 'titulo', 'id' => 'inpTitulo', 'class' => 'form-control', 'placeholder' => 'Minha postagem')); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="selectCategoria" class="col-lg-3 control-label">Categoria</label>
            <div class="col-lg-9">
                <?php echo form_dropdown('categoria', $categorias, '', 'class="form-control" id="selectCategoria"'); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inpDescricao" class="col-lg-3 control-label">Descrição</label>
            <div class="col-lg-9">
                <?php echo form_textarea(array('name' => 'descricao', 'id' => 'inpDescricao', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Breve descrição do seu link.')); ?>
            </div>
        </div>
    </div>
    <div class="passo-2" id="miniaturas"></div>
    <div class="passo-3">
        <div class="clearfix"></div>
        <div id="imagemCrop">

            <div class="col-lg-8" id="cropbox"></div>
            <div class="col-lg-4">
                <div id="preview-pane" class="post">
                    <div class="header">
                        <span class="category">Categoria</span>
                        <span class="clicks"><?php echo rand(1235, 5642); ?></span>
                    </div>
                        <div class="img preview-container">
                            <img src="#" class="jcrop-preview" alt="Preview" />
                        </div>
                    <div class="footer">
                        <span class="title">Titulo</span>
                    </div>
                </div>
            </div>
        </div>
        <?php
        echo form_input(array('name' => 'x', 'id' => 'inpX', 'type' => 'hidden'));
        echo form_input(array('name' => 'y', 'id' => 'inpY', 'type' => 'hidden'));
        echo form_input(array('name' => 'h', 'id' => 'inpH', 'type' => 'hidden'));
        echo form_input(array('name' => 'w', 'id' => 'inpW', 'type' => 'hidden'));
        echo form_input(array('name' => 'image', 'id' => 'inpImg', 'type' => 'hidden'));
        ?>
    </div>
    <div class="clearfix"></div>
    <button type="submit" form="layCadastroLink" class="btn btn-labeled btn-success pull-right">
        <span class="btn-label"><i class="fa fa-check"></i></span>Enviar
    </button>
<?php
echo form_close();
