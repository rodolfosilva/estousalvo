<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<h2 class="page_title">Meus links<?php echo anchor('painel/add_link', '<span class="btn-label"><i class="fa fa-plus"></i></span> Enviar novo link', 'class="btn btn-info btn-labeled pull-right"'); ?></h2>
<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>#</th>
            <th>Informações</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (is_array($links)) {
            $informacoes = '<strong>Site:&nbsp;</strong>%s<br />';
            $informacoes .= '<strong>Categoria:&nbsp;</strong>%s<br />';
            $informacoes .= '<strong>Titulo:&nbsp;</strong>%s<br />';
            $informacoes .= '<strong>Cliques:&nbsp;</strong>%d<br />';
            $informacoes .= '<strong>%s&nbsp;</strong>%s<br />';
            $informacoes .= '<strong>Permanlink:&nbsp;</strong>%s';
            foreach ($links as $link) {
                // $acoes = '<a href="'.site_url('painel/edit_link/'.$link['id']).'" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Editar link"><span class="glyphicon glyphicon-cog"></span></a>';
                $acoes = '';
                if ($link['published'] == 1) {
                    $acoes .= '&nbsp;<a href="'.site_url('painel/toggle_link/'.$link['id']).'" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Bloquear visualização"><span style="color:#D9534F" class="glyphicon glyphicon-eye-close"></span></a>';
                } else {
                    $acoes .= '&nbsp;<a href="'.site_url('painel/toggle_link/'.$link['id']).'" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Desbloquear visualização"><span style="color:#5CB85C" class="glyphicon glyphicon-eye-open"></span></a>';
                }

                printf(
                    '<tr><td align="center" style="vertical-align:middle"><center>%d</center></td><td>%s</td><td align="center" style="vertical-align:middle"><center>%s</center></td></tr>',
                    $link['id'],
                    sprintf(
                        $informacoes,
                        anchor('http://'.$link['site_url'], $link['site_titulo'], 'target="_blank"'),
                        $link['categoria_nome'],
                        $link['titulo'],
                        $link['cliques'],
                        ($link['post_date'] <= date('Y-m-d H:i:s') ? 'Publicado em:' : 'Agendado para:'),
                        date('d/m/Y \à\s H:i', strtotime($link['post_date'])),
                        anchor('go/'.$link['slug'], site_url('go/'.$link['slug']), 'target="_blank"')
                    ),
                    $acoes
                );
            }
        }
        ?>
    </tbody>
</table>
<?php
echo $paginacao;
