<?php
$name = isset($link['image_arquivo']) && !empty($link['image_arquivo']) ? explode('.', $link['image_arquivo']) : array();
if (isset($link['image_diretorio'], $name[0], $name[1])) {
    $imagem = $link['image_diretorio'].$name[0].'-240x200.'.$name[1];
} else {
    $imagem = 'public/img/default-240x200.jpg';
}
?>
<div class="col-sx-12 col-lg-3 col-md-4 col-sm-6">
    <div class="post">
        <div class="header">
            <?php echo anchor('categoria/'.$link['categoria_slug'], $link['categoria_nome'], 'class="category"'); ?>
            <span class="clicks"><?php echo $link['cliques']; ?></span>
        </div>
        <?php
            echo anchor(
                'go/'.$link['slug'],
                img(
                    array(
                        'src' => base_url($imagem),
                        'width' => 240,
                        'height' => 200,
                        'alt' => $link['titulo']
                    )
                ),
                array(
                    'class' => 'img',
                    'target' => '_blank'
                )
            );
        ?>
        <div class="footer">
            <?php
                echo anchor(
                    'go/'.$link['slug'],
                    $link['titulo'],
                    array(
                        'class' => 'title',
                        'target' => '_blank'
                    )
                );
            ?>
        </div>
    </div>
</div>
<div class="col-sx-12 col-lg-9 col-md-8 col-sm-6">
    <h1 style="font-size:26px;margin-top:0;"><?php echo $link['titulo']; ?></h1>
    <p><?php echo $link['descricao']; ?></p>
    <div style="margin:10px 0;">
        <?php
            echo anchor(
                'go/'.$link['slug'],
                'Ver completo',
                array(
                    'class' => 'btn btn-success btn-block',
                    'target' => '_blank'
                )
            );
        ?>
    </div>
    <div>
        <strong><?php echo anchor('categoria/'.$link['categoria_slug'], $link['categoria_nome'], 'class="pull-left"'); ?></strong>
        <time class="pull-right small"><?php echo date('d/m/Y H:i', strtotime($link['post_date'])); ?></time>
    </div>
</div>
