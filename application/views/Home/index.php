<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<div class="col-sm-12" id="tabLinks">
    <?php $sufix_page = isset($current_categoria) ? 'categoria/'.$current_categoria : '' ; ?>
    <div class="btn-group btn-group-justified">
        <?php echo anchor('/'.$sufix_page, 'Mais clicados', array('class' => 'btn btn-block '.($current_page == 'home/index' ? 'btn-primary' : 'btn-default'))); ?>
        <?php echo anchor('ultimos_enviados/'.$sufix_page, 'Últimos enviados', array('class' => 'btn btn-block '.($current_page == 'home/ultimosenviados' ? 'btn-primary' : 'btn-default'))); ?>
    </div>
</div>
<div class="posts">
    <?php
    if (isset($links) && is_array($links)) {
        $total = count($links);
        $open_div = false;
        $i = 0;
        foreach ($links as $link) {
            $name = isset($link['image_arquivo']) && !empty($link['image_arquivo']) ? explode('.', $link['image_arquivo']) : array();
            if (isset($link['image_diretorio'], $name[0], $name[1])) {
                $imagem = $link['image_diretorio'].$name[0].'-240x200.'.$name[1];
            } else {
                $imagem = 'public/img/default-240x200.jpg';
            }
    ?>
    <div class="col-sx-12 col-lg-3 col-md-4 col-sm-6">
        <div class="post">
            <div class="header">
                <?php echo anchor('categoria/'.$link['categoria_slug'], $link['categoria_nome'], 'class="category"'); ?>
                <span class="clicks"><?php echo $link['cliques']; ?></span>
            </div>
            <?php
                echo anchor(
                    'go/'.$link['slug'],
                    img(
                        array(
                            'src' => base_url($imagem),
                            'width' => 240,
                            'height' => 200,
                            'alt' => $link['titulo']
                        )
                    ),
                    array(
                        'class' => 'img',
                        'target' => '_blank'
                    )
                );
            ?>
            <div class="footer">
                <?php
                    echo anchor(
                        'go/'.$link['slug'],
                        $link['titulo'],
                        array(
                            'class' => 'title',
                            'target' => '_blank'
                        )
                    );
                ?>
            </div>
        </div>
    </div>
    <?php
        }
    }
    ?>
</div>
<div class="clearfix"></div>
<?php
echo isset($paginacao) && !empty($paginacao) ? '<div class="row" style="text-align:center;">'.$paginacao.'</div>': '';
