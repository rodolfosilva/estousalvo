<?php defined('BASEPATH') or exit('No direct script access allowed');
echo form_open('/reset_senha/'.$token, array('class' => 'ajaxSubmit'));
?>
    <h2 class="page_title">Cadastrar nova senha</h2>
    <fieldset class="form-group">
        <label class="control-label" for="ResetNovaSenha">Nova Senha:</label>
        <div class="controls">
            <input type="password" class="form-control" placeholder="Digite sua nova senha" id="ResetNovaSenha" name="senha" autocomplete="off">
        </div>
    </fieldset>
    <fieldset class="form-group">
        <label class="control-label" for="ResetRepetNovaSenha">Repita Senha:</label>
        <div class="controls">
            <input type="password" class="form-control" placeholder="Repita sua nova senha" id="ResetRepetNovaSenha" name="repete_senha" autocomplete="off">
            <input type="hidden" value="<?php echo $token; ?>" name="token">
        </div>
    </fieldset>
    <button type="submit" class="btn btn-labeled btn-success pull-right">
        <span class="btn-label"><i class="fa fa-check"></i></span>Resetar
    </button>
<?php
echo form_close();
