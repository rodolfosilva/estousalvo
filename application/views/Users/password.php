<?php defined('BASEPATH') or exit('No direct script access allowed');
echo form_open('painel/password', array('class' => 'form-horizontal ajaxSubmit'));
?>
    <h2 class="page_title">Alterar minha senha</h2>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="RegistroSenhaAtual">Senha atual</label>
        <div class="col-lg-9">
            <input type="password" id="RegistroSenhaAtual" name="senha_atual" class="form-control" placeholder="Senha atual de acesso">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="RegistroSenha">Senha</label>
        <div class="col-lg-9">
            <input type="password" id="RegistroSenha" name="senha" class="form-control" placeholder="Nova senha de acesso">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="RegistroRepeteSenha">Repita a senha</label>
        <div class="col-lg-9">
            <input type="password" id="RegistroRepeteSenha" name="repete_senha" class="form-control" placeholder="Repita sua nova senha de acesso">
        </div>
    </div>
    <button type="submit" class="btn btn-labeled btn-success pull-right">
        <span class="btn-label"><i class="fa fa-check"></i></span>Salvar
    </button>
<?php
echo form_close();
