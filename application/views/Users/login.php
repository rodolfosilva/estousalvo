<?php defined('BASEPATH') or exit('No direct script access allowed');
echo form_open('/login', array('class' => 'form-horizontal ajaxSubmit'));
?>
    <h2 class="page_title">Entrar<?php echo isset($facebook_login_url) && !empty($facebook_login_url) ? anchor($facebook_login_url, '<i class="fa fa-facebook"></i> Logar usando o facebook', 'class="btn btn-facebook btn-social pull-right"') : ''; ?></h2>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="LoginUsername">Usuário ou email</label>
        <div class="col-lg-9">
            <input type="text" class="form-control" placeholder="nome@dominio.com.br" id="LoginUsername" name="username">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="LoginPassword">Senha</label>
        <div class="col-lg-9">
            <input type="password" class="form-control" autocomplete="off" placeholder="Senha de acesso" id="LoginPassword" name="password">
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-3 control-label"></div>
        <div class="col-lg-9">
            <?php echo anchor('recuperar_senha', 'Esqueceu a senha?', 'class="btn btn-link"'); ?>
            <label class="checkbox">
                <!-- <input type="checkbox" name="remember"> Continuar logado ¦ -->
            </label>
        </div>
    </div>
    <button type="submit" class="btn btn-labeled btn-success pull-right">
        <span class="btn-label"><i class="fa fa-check"></i></span>Entrar
    </button>
<?php
echo form_close();
