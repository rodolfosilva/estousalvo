<?php defined('BASEPATH') or exit('No direct script access allowed');
echo form_open('/recuperar_senha', array('class' => 'ajaxSubmit'));
?>
    <h2 class="page_title">Esqueceu sua senha?</h2>
    <fieldset class="form-group">
        <label class="control-label" for="RecuperarSenhaEmail">Digite seu e-mail:</label>
        <div class="controls">
            <input type="text" class="form-control" placeholder="nome@dominio.com.br" id="RecuperarSenhaEmail" name="email" autocomplete="on">
        </div>
    </fieldset>
    <div class="separator">
    &mdash;OU&mdash;<br><br>
    </div>
    <fieldset class="form-group">
        <label class="control-label" for="RecuperarSenhaUsuario">Digite seu nome de usuário:</label>
        <div class="controls">
          <span class="input-group">
            <span class="input-group-addon">@</span>
            <input type="text" class="form-control" placeholder="" id="RecuperarSenhaUsuario" name="usuario" autocomplete="on">
          </span>
        </div>
    </fieldset>
    <button type="submit" class="btn btn-labeled btn-success pull-right">
        <span class="btn-label"><i class="fa fa-check"></i></span>Recuperar
    </button>
<?php
echo form_close();
