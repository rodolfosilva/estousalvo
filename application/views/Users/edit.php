<?php defined('BASEPATH') or exit('No direct script access allowed');
echo form_open('painel/configuracoesDaConta', array('class' => 'form-horizontal ajaxSubmit'));
?>
    <h2 class="page_title">Alterar minha conta</h2>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="RegistroNome">Nome completo</label>
        <div class="col-lg-9">
            <input type="text" value="<?php echo $user['nome'] ?>" class="form-control" id="RegistroNome" name="nome" placeholder="Seu nome completo">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="RegistroEmail">Email</label>
        <div class="col-lg-9">
            <input type="text" value="<?php echo $user['email'] ?>" id="RegistroEmail" class="form-control" name="email" placeholder="nome@dominio.com.br">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="RegistroUser">Usuário</label>
        <div class="col-lg-9">
            <input type="text" value="<?php echo $user['username'] ?>" id="RegistroUser" name="usuario" class="form-control" placeholder="nome_de_usuario">
        </div>
    </div>
    <button type="submit" class="btn btn-labeled btn-success pull-right">
        <span class="btn-label"><i class="fa fa-check"></i></span>Salvar
    </button>
<?php
echo form_close();
