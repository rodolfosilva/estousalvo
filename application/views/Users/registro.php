<?php defined('BASEPATH') or exit('No direct script access allowed');
echo form_open('/registro', array('class' => 'form-horizontal ajaxSubmit'));
?>
    <h2 class="page_title">Cadastrar-me<?php echo isset($facebook_login_url) && !empty($facebook_login_url) ? anchor($facebook_login_url, '<i class="fa fa-facebook"></i> Logar usando o facebook', 'class="btn btn-social btn-facebook pull-right"') : ''; ?></h2>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="RegistroNome">Nome completo</label>
        <div class="col-lg-9">
            <input type="text" class="form-control" id="RegistroNome" name="nome" placeholder="Seu nome completo">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="RegistroEmail">Email</label>
        <div class="col-lg-9">
            <input type="text" id="RegistroEmail" class="form-control" name="email" placeholder="nome@dominio.com.br">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="RegistroUser">Usuário</label>
        <div class="col-lg-9">
            <input type="text" id="RegistroUser" name="usuario" class="form-control" placeholder="nome_de_usuario">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="RegistroSenha">Senha</label>
        <div class="col-lg-9">
            <input type="password" id="RegistroSenha" name="senha" class="form-control" placeholder="Senha de acesso">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="RegistroRepeteSenha">Repita a senha</label>
        <div class="col-lg-9">
            <input type="password" id="RegistroRepeteSenha" name="repete_senha" class="form-control" placeholder="Repita sua senha de acesso">
        </div>
    </div>
    <button type="submit" class="btn btn-labeled btn-success pull-right">
        <span class="btn-label"><i class="fa fa-check"></i></span>Cadastrar
    </button>
<?php
echo form_close();
