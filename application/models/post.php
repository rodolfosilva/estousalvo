<?php defined('BASEPATH') or exit('No direct script access allowed');

class Post extends AppModel
{
    protected $table = 'posts';

    public function getAll($limit = null, $offset = null, $published = true)
    {
        $this->db->order_by($this->table.'.created', 'DESC');
        return parent::getAll($limit, $offset, $published);
    }
}

/* End of file post.php */
/* Location: ./application/modules/Posts/models/post.php */
