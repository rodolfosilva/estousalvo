<?php defined('BASEPATH') or exit('No direct script access allowed');

class Imagem extends AppModel
{
    protected $table = 'imagens';

    public function getOptions($id)
    {
        $this->db->select(array('id','options'));
        $result = $this->getById($id);

        if (isset($result['options']) && !empty($result['options'])) {
            return json_decode($result['options'], true);
        } else {
            return array();
        }
    }

    public function setOptions($id, $options = array())
    {
        if ($id != null) {
            return $this->edit(
                array('options' => json_encode($options)),
                $id
            );
        } else {
            return false;
        }
    }

    public function getByMd5($id)
    {
        return (!empty($id)) ? $this->getBy('md5('.$this->table.'.id)', $id) : false;
    }

    public function addOrChangeOption($args, $id)
    {
        if ($id != null) {
            $db_options = $this->getOptions($id);
            $new_options = json_encode(array_merge_recursive($args, $db_options));
            return $this->setOptions($id, $new_options) ? true : false;
        } else {
            return false;
        }
    }
}

/* End of file imagem.php */
/* Location: ./application/models/imagem.php */
