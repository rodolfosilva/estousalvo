<?php defined('BASEPATH') or exit('No direct script access allowed');

class User extends AppModel
{
    protected $table = 'users';

    public function getAll()
    {
        $this->db->order_by($this->table.'.nome', 'ASC');
        return parent::getAll();
    }

    public function uniqueByEmail($email, $id = null)
    {
        $this->db->where($this->table.'.email', $email);
        if (!empty($id)) {
            $this->db->where($this->table.'.id !=', $id);
        }
        return $this->getCount(false);
    }

    public function uniqueByUsername($username, $id = null)
    {
        $this->db->where($this->table.'.username', $username);
        if (!empty($id)) {
            $this->db->where($this->table.'.id !=', $id);
        }
        return $this->getCount(false);
    }

    public function getByEmail($email = null)
    {
        if (!empty($email)) {
            return $this->getBy($this->table.'.email', $email);
        }
        return false;
    }

    public function getByFacebookId($id = null)
    {
        if (!empty($id)) {
            $this->db->select(
                array(
                   $this->table.'.id',
                   $this->table.'.nome',
                   $this->table.'.email',
                   $this->table.'.username'
                )
            );
            return $this->getBy($this->table.'.facebook_id', $id);
        }
        return false;
    }

    public function getByToken($token = null)
    {
        if (!empty($token)) {
            return $this->getBy($this->table.'.token', $token);
        }
        return false;
    }

    public function encript($string)
    {
        return md5($string);
        //return md5(sha1(md5(sha1(md5($string)))));
    }

    public function checkUser($user, $pass)
    {
        $this->db->select(
            array(
                'id',
                'nome',
                'email',
                'username'
            )
        );
        $this->db->where($this->table.(preg_match('/^[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\-]+\.[a-z]{2,4}$/', $user) ? '.email' : '.username'), $user);
        return $this->getBy($this->table.'.password', $this->encript($pass));
    }

    public function setUserSession($id)
    {
        if ($this->config->item('sess_use_database')) {
            $this->db->where('session_id', $this->session->userdata('session_id'));
            $this->db->update($this->config->item('sess_table_name'), array('user_id' => $id));
        }
    }

    public function deleteUserRoles($id, $role_id)
    {
        return $this->db->delete('user_roles', array('userID' => $id, 'roleID' => $role_id));
    }

    public function deleteUserPerms($id, $perm_id)
    {
        return $this->db->delete('user_perms', array('userID' => $id, 'permID' => $perm_id));
    }

    public function resetarSenha($usuario, $email)
    {
        $this->db->where('username', $usuario);
        $this->db->or_where('email', $email);
        $this->db->limit(1);
        $query = $this->db->get($this->table);
        if ($query->num_rows == 1) {
            $hasrest = md5(uniqid(mt_rand(), true));
            $result = $query->result_array();
            $this->edit(array('token' => $hasrest), $result[0]['id']);
            return array(
                'token' => $hasrest,
                'id' => $result[0]['id'],
                'nome' => $result[0]['nome'],
                'email' => $result[0]['email'],
                'username' => $result[0]['username'],
            );
        } else {
            return false;
        }
    }
}

/* End of file user.php */
/* Location: ./application/modules/Users/models/user.php */
