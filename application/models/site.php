<?php defined('BASEPATH') or exit('No direct script access allowed');

class Site extends AppModel
{
    protected $table = 'sites';

    public function getAll($limit = null, $offset = null, $published = false)
    {
        $this->db->join($this->table, $this->table.'.id = user_sites.site_id');
        $this->db->join('imagens', $this->table.'.imagem_id = imagens.id', 'left');
        $this->db->where($this->table.'.published >', 0);
        return parent::getAll($limit, $offset, $published, 'user_sites');
    }

    public function confirmaToken($token = null)
    {
        $result = false;
        if ($token != false) {
            $this->db->where('user_sites.token', $token);
            return $this->db->update('user_sites', array('confirmacao' => 1));
        }
        return $result;
    }

    public function getSitesByUser($id = null)
    {
        $this->db->select('user_sites.user_id, user_sites.confirmacao, user_sites.token, sites.*, imagens.diretorio AS img_dir, imagens.arquivo AS img_arquivo');
        $this->db->join('users', 'users.id = user_sites.user_id');
        $this->db->where('users.id', $id);
        $this->db->order_by($this->table.'.titulo', 'ASC');
        $this->db->group_by($this->table.'.id', 'ASC');
        $results = $this->getAll(null, null, false);
        if ($results) {
            foreach ($results as $key => $value) {
                $results[$key]['site_img'] = $this->defaultSiteImg($value['url'], trim($value['img_dir'].$value['img_arquivo']));
            }
        }
        return $results;
    }

    public function getBy($key, $value = null)
    {
        $this->db->select(
            array(
                $this->table.'.id',
                'user_sites.user_id',
                'user_sites.confirmacao',
                'user_sites.token',
                $this->table.'.url',
                $this->table.'.titulo',
                $this->table.'.published',
                'imagens.diretorio AS img_dir',
                'imagens.arquivo AS img_arquivo'
            )
        );
        $this->db->group_by($this->table.'.id', 'ASC');
        $result = parent::getBy($key, $value);
        if ($result) {
            $result['site_img'] = $this->defaultSiteImg($result['url'], trim($result['img_dir'].$result['img_arquivo']));
        }
        return $result;
    }

    public function getById($id = null)
    {
        if (!empty($id)) {
            return $this->getBy('user_sites.site_id', $id);
        }
        return  false;
    }

    public function getByToken($token = null)
    {
        if (!empty($token)) {
            return $this->getBy('user_sites.token', $token);
        }
        return  false;
    }

    public function addNewSite($user_id, $domain, $titulo)
    {
        $this->db->trans_begin();
        try {
            $data = array(
                'titulo' => $titulo,
                'url' => $domain,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            );
            if ($site_id = $this->add($data)) {
                $this->db->insert('user_sites', array('user_id' => $user_id, 'site_id' => $site_id));
            } else {
                throw new Exception("Error ao cadastrar o site", 1);
            }
            if ($this->db->trans_status() == false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return false;
        }
    }
}

/* End of file site.php */
/* Location: ./application/modules/Sites/models/site.php */
