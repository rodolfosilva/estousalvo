<?php defined('BASEPATH') or exit('No direct script access allowed');

class Link extends AppModel
{
    protected $table = 'links';

    public function __construct()
    {
        $this->default_args = array_merge($this->default_args, array('categoria' => null));
    }

    public function getAll($limit = null, $offset = null, $published = false)
    {
        $this->db->select(
            array(
                $this->table.'.id',
                $this->table.'.categoria_id',
                $this->table.'.imagem_id',
                $this->table.'.site_id',
                $this->table.'.titulo',
                $this->table.'.descricao',
                $this->table.'.url',
                $this->table.'.slug',
                $this->table.'.post_date',
                $this->table.'.created',
                $this->table.'.modified',
                $this->table.'.published',
                'sites.titulo AS site_titulo',
                'sites.url AS site_url',
                'user_sites.user_id AS user_id',
                'categorias.nome AS categoria_nome',
                'categorias.slug AS categoria_slug',
                'MD5(imagens.id) AS image_md5',
                'imagens.arquivo AS image_arquivo',
                'imagens.diretorio AS image_diretorio',
                'imagens.altura AS image_height',
                'imagens.largura AS image_width',
                'imagens.tamanho AS image_size',
                'CONCAT(imagens.diretorio, imagens.arquivo) AS image_url',
                'imagens.tipo AS image_type',
                $this->table.'.cliques',
                // '(SELECT COUNT(*) FROM link_cliques WHERE link_id = links.id) AS cliques',
                // '(COUNT(*) - 1) AS cliques',
            )
        );
        $this->db->join('categorias', $this->table.'.categoria_id = categorias.id');
        $this->db->join('sites', $this->table.'.site_id = sites.id');
        $this->db->join('user_sites', 'sites.id = user_sites.site_id');
        $this->db->join('imagens', $this->table.'.imagem_id = imagens.id AND imagens.published = 1', 'left');
        // $this->db->join('link_cliques', $this->table.'.id = link_cliques.link_id', 'left');
        $this->db->group_by(array($this->table.'.id'));
        if (!$published) {
            $this->db->where($this->table.'.published >', 0);
        }
        // $this->db->where('imagens.published', true);
        $this->db->where('categorias.published', true);
        return parent::getAll($limit, $offset, $published);
    }

    public function totalLinksByUser($id = null)
    {
        $this->db->join('categorias', $this->table.'.categoria_id = categorias.id');
        $this->db->join('sites', 'sites.id = links.site_id');
        $this->db->where('categorias.published', true);
        $this->db->where($this->table.'.site_id IN (SELECT site_id FROM user_sites WHERE user_id = ' . $id . ')');
        return $this->getCount();
    }

    public function getLinksByUser($id = null, $limit = null, $offset = null)
    {
        $this->db->where($this->table.'.site_id IN (SELECT site_id FROM user_sites WHERE user_id = ' . $id . ')');
        $this->db->order_by($this->table.'.created', 'DESC');
        $this->db->order_by($this->table.'.id', 'DESC');
        return $this->getAll($limit, $offset);
    }

    public function getByBusca($buscar, $categoria = null, $limit = null, $page = null)
    {
        if (!empty($buscar)) {
            $this->db->like($this->table.'.titulo', $buscar);
            // $this->db->or_like($this->table.'.descricao', $buscar);
            return $this->getNovosByClicks($categoria, $limit, $this->offset($limit, $page));
        }
        return array();
    }

    public function totalByBusca($buscar, $categoria = null)
    {
        if (!empty($buscar)) {
            $this->db->like($this->table.'.titulo', $buscar);
            // $this->db->or_like($this->table.'.descricao', $buscar);
            return $this->getCountByNovos($categoria);
        }
        return 0;
    }

    private function setCategoria($categoria)
    {
        if (!empty($categoria)) {
            if ($categoria != 'all') {
                $this->db->where('categorias.slug', $categoria);
            }
        } else {
            $this->db->where($this->table.'.categoria_id >', 1);
        }
    }

    public function getAllLink($args = array())
    {
        $args = array_merge($this->default_args, $args);

        $this->setCategoria($args['categoria']);
        $this->orderBy($args['order']);

        $this->db->where('sites.published', 1);
        $this->db->where('categorias.published', true);
        $this->db->where($this->table.'.post_date <=', date('Y-m-d H:i:s'));

        return $this->getAll($args['limit'], $args['offset'], true);
    }

    public function getNovosByClicks($categoria = null, $limit = null, $offset = null)
    {
        return $this->getAllLink(
            array(
                'order' => array(
                    'DATE(links.post_date)'   => 'DESC',
                    $this->table.'.cliques'   => 'DESC',
                    $this->table.'.post_date' => 'ASC',
                ),
                'categoria' => $categoria,
                'limit' => $limit,
                'offset' => $offset,
            )
        );
    }

    public function getCountByNovos($categoria = null)
    {
        $this->setCategoria($categoria);

        $this->db->join('categorias', $this->table.'.categoria_id = categorias.id');
        $this->db->join('sites', 'sites.id = links.site_id');

        $this->db->where('sites.published', 1);
        $this->db->where('categorias.published', true);
        $this->db->where($this->table.'.post_date <=', date('Y-m-d H:i:s'));

        return $this->getCount();
    }

    public function totalNovosByClicks($categoria = null)
    {
        return $this->getCountByNovos($categoria);
    }

    public function getNovosByUltimosEnviados($categoria = null, $limit = null, $offset = null)
    {
        return $this->getAllLink(
            array(
                'order' => array(
                    'DATE(links.post_date)'   => 'DESC',
                ),
                'categoria' => $categoria,
                'limit' => $limit,
                'offset' => $offset,
            )
        );
    }

    public function getNewLinks($id = null, $limit = 15)
    {
        if ($id != null && $id > 0) {
            $this->db->where($this->table.'.id >', $id);
        }

        $links = $this->getAllLink(
            array(
                'order' => array(
                    $this->table.'.id'   => 'DESC',
                ),
                'limit' => $limit,
            )
        );

        $new_links = array();
        if (is_array($links)) {
            foreach ($links as $link) {
                $new_links[] = array(
                    'id'             => $link['id'],
                    'titulo'         => $link['titulo'],
                    'categoria'      => $link['categoria_nome'],
                    'categoria_link' => site_url('categoria/'.$link['categoria_slug']),
                    'site'           => $link['site_titulo'],
                    'site_url'       => $link['site_url'],
                    'permanlink'     => site_url('go/'.$link['slug']),
                    'link_slug'      => $link['slug'],
                    'data'           => $link['modified'],
                    'imagem'         => base_url($link['image_url']),
                    'cliques'        => $link['cliques'],
                );
            }
        }
        return $new_links;
    }

    public function addClick($id)
    {
        $id = trim($id);
        if (!empty($id)) {
            $this->db->insert(
                'link_cliques',
                array(
                    'link_id' => $id,
                    'origem' => (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''),
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'created' => date('Y-m-d h:i:s'),
                )
            );
        }
    }

    public function getBySlug($slug)
    {
        $slug = trim($slug);
        if (!empty($slug)) {
            $this->db->select(array($this->table.'.id', $this->table.'.url'));
            return $this->getBy($this->table.'.slug', $slug);
        }
        return false;
    }

    public function getNewSlug()
    {
        $unique = false;
        do {
            $start = (int)rand(0, 26);
            $end = 5;
            $tmp = substr(md5(uniqid($_SERVER['REQUEST_TIME'].rand()).rand()), $start, $end);
            $result = $this->getBy($this->table.'.slug', $tmp);
            if (empty($result)) {
                $unique = $tmp;
            }
        } while ($unique == false);

        return $unique;
    }

    public function getByUrl($url)
    {
        $this->db->where($this->table.'.url', $url);
        $this->db->where($this->table.'.published >=', 1);
        return $this->getCount(false) >= 1 ? true : false;
    }

    public function getById($id)
    {
        return parent::getById($id);
    }
}

/* End of file link.php */
/* Location: ./application/modules/Links/models/link.php */
