<?php defined('BASEPATH') or exit('No direct script access allowed');

class Categoria extends AppModel
{
    protected $table = 'categorias';

    public function getAll($limit = null, $offset = null)
    {
        $this->db->order_by($this->table.'.nome', 'ASC');
        return parent::getAll($limit, $offset);
    }

    public function getCategoriasComConteudo()
    {
        $this->db->select($this->table.'.*, COUNT(*) AS total_links');
        $this->db->where($this->table.'.published', true);
        $this->db->join('links', $this->table.'.id = links.categoria_id AND links.published = 1');
        $this->db->group_by(array($this->table.'.id'));
        return $this->getAll();
    }

    public function getBySlug($slug = null)
    {
        return $this->getBy($this->table.'.slug', $slug);
    }
}

/* End of file categoria.php */
/* Location: ./application/models/categoria.php */
