<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ranking extends AppModel
{
    protected $table = 'rankings';

    public function getAll($limit = 15, $offset = 0)
    {
        $this->db->select(
            array(
                $this->table.'.*',
                'sites.titulo',
                'sites.url',
                'imagens.diretorio AS img_dir',
                'imagens.arquivo AS img_arquivo',
            )
        );
        $this->db->order_by($this->table.'.ranking', 'ASC');
        $this->db->join('sites', $this->table.'.site_id = sites.id');
        $this->db->join('imagens', 'sites.imagem_id = imagens.id', 'left');
        $results = parent::getAll($limit, $offset, false);
        if ($results) {
            foreach ($results as $key => $value) {
                $results[$key]['site_img'] = $this->defaultSiteImg($value['url'], trim($value['img_dir'].$value['img_arquivo']));
            }
        }
        return $results;
    }

    public function totalRanking()
    {
        $this->db->order_by($this->table.'.ranking', 'ASC');
        $this->db->join('sites', $this->table.'.site_id = sites.id');
        return $this->getCount(false);
    }
}

/* End of file ranking.php */
/* Location: ./application/modules/Rankings/models/ranking.php */
