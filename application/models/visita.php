<?php defined('BASEPATH') or exit('No direct script access allowed');

class Visita extends AppModel
{
    protected $table = 'visitas';

    public function siteVisitas($id = null)
    {
        $this->db->select(
            array(
                'id',
                'titulo',
                'domain',
                'SUM(total_visitas) AS total_visitas'
            )
        );
        $this->db->order_by('CONCAT(DATE(created) , HOUR(created))', 'DESC');
        $this->db->order_by('total_visitas', 'DESC');
        $this->db->where('id', $id);
        $this->db->limit(1);
        $this->db->group_by('id');
        return $this->db->get('view_sites_visitas')->result_array();
    }


    public function add($args = array())
    {
        if (!empty($args['domain'])) {
            $this->load->driver('cache');
            $this->cache->delete('ultimas_visitas');
        }
        return parent::add($args);
    }

    public function ultimasVisitas($limit = 9)
    {
        $this->load->driver('cache', array('adapter'=>'file'));
        $rtn = $this->cache->get('ultimas_visitas.tmp');
        if (!$rtn || empty($rtn)) {
            $this->db->select(
                array(
                    'id',
                    'titulo',
                    'domain',
                    'total_visitas',
                    'created',
                    'site_img'
                )
            );
            $this->db->order_by('CONCAT(DATE(created) , HOUR(created))', 'DESC');
            $this->db->order_by('total_visitas', 'DESC');
            $this->db->where('CONCAT(DATE(created), HOUR(created)) IN (SELECT DISTINCT MAX(CONCAT(DATE(vsv.created), HOUR(vsv.created))) FROM view_sites_visitas vsv WHERE vsv.id = view_sites_visitas.id)');
            $this->db->group_by('id');
            $this->db->limit($limit);
            $results = $this->db->get('view_sites_visitas')->result_array();

            for ($i = 0; $i < $limit; $i++) {
                $parceiro_top = array(
                    'domain' => site_url('/'),
                    'total_visitas' => 'Seja parceiro cadastre-se',
                    'titulo' => 'Seja parceiro cadastre-se no Estou Salvo',
                    'target' => '_self',
                    'imagem' => 'public/img/default-230x40.jpg',
                );
                if (isset($results[$i])) {
                    $parceiro_top = array_merge($parceiro_top, $results[$i]);
                    $parceiro_top['total_visitas'] = sprintf('Enviou %d visitas na ultima hora!', $parceiro_top['total_visitas']);
                    $parceiro_top['domain'] = 'http://'.$parceiro_top['domain'];
                    $parceiro_top['target'] = '_blank';
                    $parceiro_top['imagem'] = $this->defaultSiteImg($results[$i]['domain'], $results[$i]['site_img']);
                }
                $rtn[] = $parceiro_top;
            }
            $this->cache->file->save('ultimas_visitas.tmp', $rtn, 3600);
        }
        return $rtn;
    }
}

/* End of file visita.php */
/* Location: ./application/models/visita.php */
