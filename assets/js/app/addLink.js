;var addLink = (function($, estousalvo, feedback, undefined){
    "use strict";
    var __document = $( document ),
        CropImage = new SliderCrop("#cropbox"),
        cleanImageLocal = function() {
            var image_post = $('#image_post');
            // image_post.wrap('<form>').closest('form').get(0).reset();
            image_post.wrap('<form>').parent('form').trigger('reset');
            image_post.unwrap();
        },
        resetCrop = function(image) {
            $("#cropbox").empty().html('<div class="cropMain"></div><div class="cropSlider"></div>');
            CropImage = new SliderCrop;
            CropImage.init("#cropbox");
            CropImage.loadImg(image);
            $('#imageManipulation').show();
        };
    $("#inpLink").focus();
    __document.on( "change", "#inpLink", function() {
        if ( !estousalvo.isValidUrl( $( this ).val() ) ) {
            feedback.create( "Url invalida", "error" );
            return false;
        }
    });

    __document.on( "click", "#loadImagesUrl", function () {
        if (estousalvo.isValidUrl( $( "#inpLink" ).val() )) {
            cleanImageLocal();
            estousalvo.getImagesUrl( $( "#inpLink" ).val() );
        } else {
            feedback.create( "Url invalida", "error" );
        }
    });

    __document.on( "click", "#miniaturas .crop", function () {
        cleanImageLocal();
        var image = $( this ).find( "img" ).attr( "src" );
        $( "#inpImg" ).val( image );
        resetCrop( image );
    });


    // Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.
        __document.on( "change", "#image_post", function(){
            var imagem = this.files[0],
                types = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;
            if (imagem.type.match(types)) {
                if (imagem.size < (1024*1024)) {
                    feedback.remove();
                    var reader = new FileReader();
                    reader.readAsDataURL(imagem);
                    window.teste = reader;
                      // +"name: " + imagem.name + "\n"
                    reader.onload = function (e) {
                        resetCrop( e.target.result, true );
                    };
                } else {
                    feedback.show( 'Arquivo muito grande.', 'error' );
                }
            } else {
                feedback.show( 'Arquivo invalido, selecione uma imagem.', 'error' );
            }
        });
    } else {
        feedback.show( 'Você não podera enviar imagem da sua maquina, seu browser não tem suporte para este recurso.', 'error' );
    }
    return {};
})(jQuery, estousalvo, feedback);
