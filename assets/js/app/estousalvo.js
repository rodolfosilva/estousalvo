;var estousalvo;
estousalvo = ( function ( $, _$_, feedback, undefined ) {
    var Core;
    Core = ( function () {
        var options = {};

        function Core () {
        }

        // Cheka se a url é valida
        Core.prototype.isValidUrl = function ( url ) {
            if ( ( /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/ ).test( url ) ) {
                return true;
            } else {
                return false;
            }
        };

        Core.prototype.siteUrl = function ( url ) {
            url = url.trim();
            if ( url == "" || url == "/" ) {
                return _$_.site_url;
            } else {
                return ( _$_.site_url.substr( -1 ) == "/" ? _$_.site_url : _$_.site_url + "/" ) + ( url.substr( 0, 1 ) == "/" ? url.substr( 1 ) : url );
            }
        };

        Core.prototype.getImagesUrl = function ( url ) {
            var miniaturas;
            miniaturas = $( "#miniaturas" );
            url = url.trim();
            $.ajax( {
                url: this.siteUrl( "load/images_url.json" ),
                type: "POST",
                dataType: "json",
                data: {
                    url: url
                },
                beforeSend: function () {
                    feedback.create( "Aguarde, estamos carregando as imagens do seu link...", "load" );
                    miniaturas.empty();
                }
            }).done(function (data) {
                data = typeof data == "string" ? $.parseJSON( data ) : data;
                if ( data.imagens ) {
                    feedback.remove();
                    var totalImagens = Object.keys( data.imagens ).length,
                        imagens = [],
                        img = [],
                        str_html = "";
                    $.each( data.imagens, function ( i, v ){
                        img[ i ] = new Image();
                        img[ i ].src = v.src;
                        img[ i ].onload = function () {
                            if ( this.width >= 240 && this.height >= 200 ) {
                                $( ".passo-2" ).show();
                                miniaturas.append( "<div class=\"crop\"><img src=\"" + this.src + "\" alt=\"\" \/><\/div>" ).show();
                            }
                        }
                    } );
                    miniaturas.append();
                } else if ( data.error ) {
                    $( ".passo-2" ).hide();
                    feedback.create( data.error, "error" );
                } else {
                    feedback.remove();
                }
                // miniaturas.closest(".modal:first").modal("layout");
            });
        };

        Core.prototype.openPopUp = function(url) {
            window.open(
                url,
                "window01",
                "width=700,height=500,scrollbars=NO,top=120,left=290"
            );
        };

        return Core;
    } ) ();
    return new Core();
} ) ( jQuery, site_config, feedback );
