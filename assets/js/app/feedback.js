// Mensagem Feedback AJAX //
;var feedback;
feedback = ( function ( $, undefined ) {
    var Core;
    Core = ( function () {
        var options = {};

        options.timeclose = 15;

        function Core () {
            this.timeclose = options.timeclose;
            this.element = null;
        }

        Core.prototype.getElement = function () {
            if ( !this.element || this.element == null ) {
                // if ($('.mensagemModal:first', document).length && $('.mensagemModal:first', document).closest('.modal').is(":visible")) {
                    // window.feedback.remove();
                    // $element = $('.mensagemModal:first', document);
                // } else {
                    this.element = $("#mensagem .container");
                // }
            }
            return this.element;
        };

        Core.prototype.show = function ( _msg, _type ) {
            this.create.apply( this, arguments );
        };

        Core.prototype.create = function ( mensagem, tipo, closeButton ) {
            var textPrefix, eClass;
            closeButton = closeButton ? "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>" : "";
            textPrefix = "";
            this.timeclose = options.timeclose;
            switch ( tipo ) {
                case "error" :
                    eClass = "danger";
                    textPrefix = "Erro: ";
                break;

                case "success" :
                    eClass = "success";
                    textPrefix = "Sucesso: ";
                break;
                case "info" :
                case "load" :
                    eClass = "info";
                    if ( tipo == "load" ) {
                        this.timeclose = -1;
                        mensagem = "<center><strong>" + ( mensagem.trim() == "" ? "Carregando, aguarde..." : mensagem.trim() ) + "</strong></center>";
                    } else {
                        textPrefix = "Info.: ";
                    }
                break;
            }
            this.getElement().empty().prepend( $( "<div>", { class: "alert alert-" + eClass } ).html( "<strong>" + textPrefix + "</strong>" + mensagem + closeButton ) ).show();
            this.timeclose = this.timeclose >= 1 ? options.timeclose : -1;
        };

        Core.prototype.remove = function () {
            this.timeclose = -1;
            this.getElement().fadeOut( "slow", function () {
                $( this ).empty();
            } );
        };

        Core.prototype.redirect = function ( url ) {
            if ( url.trim() != "" ) {
                setTimeout( function () {
                    location.href = url.trim();
                }, 1500 );
            }
        };

        Core.prototype.autoClose = setInterval( function () {
            if ( this.timeclose > 0 ) {
                if ( this.timeclose == 1 ) {
                    this.remove();
                } else {
                    this.timeclose--;
                }
            }
        }, 1000 );

        return Core;

    } )( $ );

    return new Core();

} )( jQuery );
