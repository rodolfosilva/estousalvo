//     SliderCrop.js 1.6.0
//     (c) 2014 Rodolfo Silva
//     SliderCrop may be freely distributed under the MIT license.

( function () {
    // Baseline setup
    // --------------

    // Establish the root object, `window`
    var root = this;

    var isUndefined = function ( obj ) {
        return obj === void 0;
    }

    var _SliderCrop = ( function () {
        var options = {};

        // __construct
        var Core = function ( e ) {
            if ( !isUndefined( e ) ) {
                this.init( e );
            }
        }

        Core.that = Core.prototype;

        Core.that.eles = {
            ele: undefined,
            container: undefined,
            img: undefined,
            overlay: undefined
        };

        Core.that.img = undefined;

        Core.that.imgInfo = {};

        Core.that.init = function ( e ) {
            this.imgInfo = {
                aw: 0,
                ah: 0,
                w: 0,
                h: 0,
                at: 0,
                al: 0,
                t: 0,
                l: 0,
                s: 1
            };
            this.settings = {
                slider: e + " .cropSlider"
            };
            var e = $(e + " .cropMain"),
                t, n, r, i = this;
            n = $("<div />").attr({
                "class": "crop-container"
            }).css({
                width: e.width(),
                height: e.height()
            });
            t = $("<img />").attr({
                "class": "crop-img"
            }).css({
                zIndex: 999,
                top: 0,
                left: 0
            });
            r = $("<div />").attr({
                "class": "crop-overlay"
            }).css({
                zIndex: 1000
            });
            n.append(r);
            n.append(t);
            e.append(n);
            this.eles.ele = e;
            this.eles.container = n;
            this.eles.img = t;
            this.eles.overlay = r;
            n.resize(function () {
                i.imgSize()
            });
            r.bind(document.ontouchstart !== null ? "mousedown" : "touchstart", function (e) {
                var t = $(this),
                    n = {
                        x: e.pageX || e.originalEvent.pageX,
                        y: e.pageY || e.originalEvent.pageY
                    },
                    s = {
                        x: t.parent().offset().left,
                        y: t.parent().offset().top
                    };
                e.preventDefault();
                $(document).bind(document.ontouchmove !== null ? "mousemove" : "touchmove", function (e) {
                    if (e.pageX || typeof e.originalEvent.changedTouches[0] !== undefined) {
                        var r = {
                            x: e.pageX || e.originalEvent.changedTouches[0].pageX,
                            y: e.pageY || e.originalEvent.changedTouches[0].pageY
                        };
                        if (parseInt(t.css("top")) == 0) {
                            t.css({
                                top: i.eles.ele.offset().top,
                                left: i.eles.ele.offset().left
                            })
                        }
                        i.imgMove({
                            t: parseInt(t.css("top")) - (s.y - (n.y - r.y)),
                            l: parseInt(t.css("left")) - (s.x - (n.x - r.x))
                        });
                        t.css({
                            left: s.x - (n.x - r.x),
                            top: s.y - (n.y - r.y)
                        })
                    }
                });
                $(document).bind(document.ontouchend !== null ? "mouseup" : "touchend", function (e) {
                    $(document).unbind(document.ontouchmove !== null ? "mousemove" : "touchmove");
                    r.css({
                        top: 0,
                        left: 0
                    })
                });
                return false
            });
            this.slider();
        };

        Core.that.loadImg = function (e) {
            var t = this;
            this.eles.img.attr("src", e).load(function () {
                t.imgSize();
            })
        };

        Core.that.imgSize = function () {
            var e = this.eles.img,
                t = {
                    w: e.css("width", "").width(),
                    h: e.css("height", "").height()
                },
                n = this.eles.container;
            var r = {
                wh: this.eles.container.width() / this.eles.container.height(),
                hw: this.eles.container.height() / this.eles.container.width()
            };
            this.imgInfo.aw = t.w;
            this.imgInfo.ah = t.h;
            if (t.w * r.hw < t.h * r.wh) {
                this.imgInfo.w = n.width() - 20 * 2;
                this.imgInfo.h = this.imgInfo.w * (t.h / t.w);
                this.imgInfo.al = 20
            } else {
                this.imgInfo.h = n.height() - 20 * 2;
                this.imgInfo.w = this.imgInfo.h * (t.w / t.h);
                this.imgInfo.at = 20
            }
            this.imgResize();
            this.updatePreviw();
        };

        Core.that.imgResize = function (e) {
            var t = this.eles.img,
                n = this.imgInfo,
                r = n.s;
            n.s = e || n.s;
            t.css({
                width: n.w * n.s,
                height: n.h * n.s
            });
            this.imgMove({
                t: -(n.h * r - n.h * n.s) / 2,
                l: -(n.w * r - n.w * n.s) / 2
            });
            this.updatePreviw();
        };

        Core.that.imgMove = function (e) {
            var t = this.eles.img,
                n = this.imgInfo,
                r = this.eles.container;
            n.t += e.t;
            n.l += e.l;
            var i = n.at - n.t,
                s = n.al - n.l;
            if (i > 20) {
                i = 20;
                n.t = n.at == 20 ? 0 : -20
            } else if (i < -(n.h * n.s - (r.height() - 20))) {
                i = -(n.h * n.s - (r.height() - 20));
                n.t = n.at == 20 ? n.h * n.s - (r.height() - 40) : n.h * n.s - (r.height() - 20)
            }
            if (s > 20) {
                s = 20;
                n.l = n.al == 20 ? 0 : -20
            } else if (s < -(n.w * n.s - (r.width() - 20))) {
                s = -(n.w * n.s - (r.width() - 20));
                n.l = n.al == 20 ? n.w * n.s - (r.width() - 40) : n.w * n.s - (r.width() - 20)
            }
            t.css({
                top: i,
                left: s
            });
            this.updatePreviw();
        };

        Core.that.slider = function () {
            var e = this;
            $(this.settings.slider).noUiSlider({
                range: [1, 4],
                start: 1,
                step: .002,
                handles: 1,
                slide: function () {
                    var t = $(this).val();
                    e.imgResize(t);
                    e.updatePreviw();
                }
            })
        };

        Core.that.coordinates = function () {
            var n = this.imgInfo,
                r = this.eles.container,
                i = this.eles.img;
                return {
                    x: -(parseInt(i.css("left")) - 20) * (n.aw / (n.w * n.s)),
                    y: -(parseInt(i.css("top")) - 20) * (n.ah / (n.h * n.s)),
                    w: (r.width() - 20 * 2) * (n.aw / (n.w * n.s)),
                    h: (r.height() - 20 * 2) * (n.ah / (n.h * n.s)),
                    image: i.attr("src")
                };
        };

        Core.that.updatePreviw = function(){
            $( "#inpW" ).val( this.coordinates().w ); // x1
            $( "#inpH" ).val( this.coordinates().h ); // y1
            $( "#inpX" ).val( this.coordinates().x ); // x2
            $( "#inpY" ).val( this.coordinates().y ); // y2
        };
        return Core;
    } )();
    root.SliderCrop = _SliderCrop;
} ).call( this );
