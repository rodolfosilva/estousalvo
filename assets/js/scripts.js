﻿jQuery(document).ready(function($){
    "use strict";
    var backToTop = $('.back-to-top');
    if (backToTop.length) {
        $(window).scroll(function() {
            if ($(this).scrollTop() > $('#cabecalho').height()) {
                backToTop.fadeIn(500);
            } else {
                backToTop.fadeOut(500);
            }
        });

        backToTop.on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({scrollTop: 0}, 500);
            return false;
        });
    }
    // ------------------------------ //
    //         Funções globais        //
    // ------------------------------ //
    $('#menu-topo, #mensagem, #wrap').affix({
        offset: {
            top: $('#cabecalho').height()
        }
    });

    // Busca os links
    $( '.buscar-links' ).on( 'submit', function (e) {
        e.preventDefault();
        var root   = $( this ),
            buscar = root.find('input[name=buscar]');

        if ( buscar && $.trim( buscar.val() ) != '' ) {
            var categoria = root.find('#selectCategoria');
            categoria = categoria.length ? (categoria.val() !=  '' ? '/' + categoria.val() : '') : '';
            window.location = estousalvo.siteUrl( '/buscar/' + encodeURIComponent( buscar.val() ) + categoria );
        } else if ( buscar ) {
            buscar.focus();
        }
    } );

    // Scroll personalizado...
    var $scrollable = $(".scrollable");
    $scrollable.jScrollPane({
        horizontalGutter:5,
        verticalGutter:5,
        mouseWheelSpeed: 18,
        autoReinitialise: true,
        showArrows: false
    });
    /*var $jspDrag = $(".jspDrag").hide();
    $scrollable.mouseenter(function(){
        $jspDrag.stop(true, true).fadeIn("slow");
    }).mouseleave(function(){
        $jspDrag.stop(true, true).fadeOut("slow");
    });*/
    // tooltip demo
    $('body').tooltip({
      selector: "[data-toggle=tooltip]",
      container: "body"
    })

    // Envio de formulários
    $(".ajaxSubmit").on('submit', function (e) {
        e.preventDefault();
        var root, buttons, upload, progress, progress_bar, progress_percent;
        root             = $(this);
        buttons          = root.find('input[type=submit], button[type=submit]');
        buttons.attr("disabled", "disabeld");

        progress         = root.find('.progress');
        progress_bar     = progress.find('.progress-bar');
        progress_percent = progress_bar.find('span');

        root.ajaxSubmit({
            delegation: true,
            beforeSubmit: function() {
                feedback.create("", "load");
                if (progress.length) {
                    progress_bar.width('0%');
                    progress_percent.html('0%');
                }
            },
            uploadProgress: function(event, position, total, percentComplete) {
                if (progress.length) {
                    var pVel = percentComplete + '%';
                    progress_bar.width(pVel);
                    progress_percent.html(pVel);
                }
            },
            success: function(response, statusText, xhr, $form) {
                response = typeof response == "string" ? $.parseJSON(response) : response;
                if (response["success"]) {
                    feedback.create(response["success"], "success");
                    if (response["close_modal"]) {
                        if ($form.hasClass('modal')) {
                            $element = $form;
                        } else {
                            $element = $form.closest('.modal:first');
                        }
                        $element.modal('hide');
                        if ($element.attr('id') == 'modalLogin') {
                            $element.modal('hide').remove();
                            feedback.remove();
                        }
                    }
                } else if (response["error"]) {
                    if (response["login"]) {
                        getLoginModal();
                    }
                    feedback.create(response["error"], "error");
                } else {
                    feedback.remove();
                }

                if (response["redirect"]) {
                    feedback.redirect(response["redirect"]);
                }
                buttons.removeAttr("disabled");
            }
        });
        return false;
    });


    Date.prototype.format = function(format) {
        var o = {
            "M+" : this.getMonth()+1, //month
            "d+" : this.getDate(),    //day
            "h+" : this.getHours(),   //hour
            "i+" : this.getMinutes(), //minute
            "s+" : this.getSeconds(), //second
            "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
            "S" : this.getMilliseconds() //millisecond
        }

        if(/(y+)/.test(format)) {
            format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("("+ k +")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
            }
        }
        return format;
    };

    var $modal = $("#ajax-modal");
    $(".modal_load").off("click").on("click", function(){
        var url = $(this).attr('href');
        $("body").modalmanager("loading");
        setTimeout(function(){
            $modal.load(url, function(){
                $modal.modal();
                // $('.nav-tabs a', $modal).on('click', function (e) {
                //     e.preventDefault();
                //     $(this).tab('show');
                //     if ($(this).closest(".modal:first")) {
                //         $(this).closest(".modal:first").modal("layout");
                //     }
                // });
            });
        }, 1000);
        return false;
    });
    var getLoginModal = window.getLoginModal = function(logado) {
        logado = !($('#topMenu').find('li a.loginModal').length);
        html = '';
        html += '<form action="' + estousalvo.siteUrl('/login') + '" method="post" id="modalLogin" class="modal ajaxSubmit">';
            html += '<div class="modal-dialog">';
                html += '<div class="modal-content">';
                    html += '<div class="modal-header">';
                    if (!logado) {
                        html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
                    }
                        html += '<h4 class="modal-title">Login</h4>';
                    html += '</div>';
                    html += '<div class="modal-body">';
                        html += '<div class="mensagemModal"></div>';
                        html += '<div class="control-group">';
                            html += '<label class="control-label" for="LoginUsername">Usuário ou email</label>';
                            html += '<div class="controls">';
                                html += '<input type="text" class="form-control" placeholder="nome@dominio.com.br" id="LoginUsername" name="username">';
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="control-group">';
                            html += '<label class="control-label" for="LoginPassword">Senha</label>';
                            html += '<div class="controls">';
                                html += '<input type="password" class="form-control" autocomplete="off" placeholder="Senha de acesso" id="LoginPassword" name="password">';
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';
                    html += '<div class="modal-footer">';
                    if (logado) {
                        html += '<input type="hidden" name="logado" value="true">';
                        html += '<a class="btn  btn-default" href="' + estousalvo.siteUrl('/') + '" aria-hidden="true">Cancelar</a>';
                    } else {
                        html += '<span class="btn  btn-default" data-dismiss="modal" aria-hidden="true">Cancelar</span>';
                    }
                        html += '<button class="btn btn-primary" type="submit">Entrar</button>';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        html += '</form>';
        $('body').prepend(html);
        if (logado) {
            $('#modalLogin').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });
        } else {
            $('#modalLogin').modal('show');
        }

    };
    $(document).on('click', '.loginModal', function () {
        getLoginModal();
        return false;
    });
    // ------------------------------ //
    //       Funções especificas      //
    // ------------------------------ //
    var $timeline = $("#timeline");
    if ($timeline.length) {
        var novosLinks = setInterval(function(){
            if ($timeline.find(".item").length) {
                var $id = $timeline.find(".item:first").attr("data-id");
                    $id = $id ? $id : 0;
            } else {
                var $id = 0;
            }
            $.ajax({
                url: estousalvo.siteUrl("load/novos_links.json"),
                type: "POST",
                dataType: "json",
                data: {
                    id: $id
                },
            }).done(function (data) {
                data = typeof data == "string" ? $.parseJSON(data) : data;
                if (data.novos_links) {
                    var $rtn = "";
                    $.each(data.novos_links, function(i, v){
                        $rtn += "<div class=\"item\" data-id=\"" + v.id + "\">"+
                                "<div class=\"top\"><img alt=\"favicon\" src=\"http://www.google.com/s2/favicons?domain=" + v.site_url + "\" height=\"16\" width=\"16\" />" + v.site +"</div>"+
                                "<a class=\"title\" target=\"_blank\" href=\"" + v.permanlink + "\">" + v.titulo + "</a>"+
                                "<div class=\"footer\">"+
                                "<a href=\"#\" class=\"category\">" + v.categoria + "</a>"+
                                "<time class=\"date\">" + new Date(Date.parse(v.data)).format("dd/MM/yyyy hh:ii:ss") + "</time>"+
                                "</div>"+
                                "<div class=\"clearfix\"></div>"+
                                "</div>";
                    });
                    $timeline.data('jsp').getContentPane().prepend($rtn);
                }
            });
        }, 10000);
    }

    var $parceiros = $("#parceiros");
    if ($parceiros.length) {
        var ultimas_visitas = function(){
            $.ajax({
                url: estousalvo.siteUrl("load/ultimas_visitas.json"),
                type: "POST",
                dataType: "json"
            }).done(function (data) {
                data = typeof data == "string" ? $.parseJSON(data) : data;
                if (data.length) {
                    var $rtn = "";
                    $.each(data, function(i, v){
                        $rtn += "<div class=\"col-md-4\">"+
                                    "<a class=\"parceiro\" target=\"" + v.target + "\" title=\"" + v.titulo + "\" href=\"" + v.domain + "\"><img src=\"" + v.imagem + "\" width=\"230\" height=\"40\" alt=\"" + v.titulo + "\" title=\"" + v.titulo + "\"/><span>" + v.total_visitas + "</span></a>"+
                                "</div>";
                    });
                    $parceiros.html($rtn);
                }
            });
        }
        var interval_ultimas = setInterval(ultimas_visitas, 120000);
        ultimas_visitas();
    }

    $('.confirmarSite').on('click', function(){
        var $this = $(this);
        $.ajax({
            url: estousalvo.siteUrl("load/autoria_site.json"),
            type: "POST",
            dataType: "json",
            data: {
                domain: $this.attr('data-domain')
            },
            beforeSend: function() {
                feedback.create("Aguarde, estamos verificando a autoria do link...", "load");
            }
        }).done(function (data) {
            data = typeof data == "string" ? $.parseJSON(data) : data;
            if (data.success) {
                feedback.create(data.success, "success");
            } else if (data.error) {
                feedback.create(data.error, "error");
            } else {
                feedback.remove();
            }

            if (data.redirect) {
                feedback.redirect(data.redirect);
            }
        });
        return false;
    });
    if ($('#inpData').length) {
        $('#agendarPublicacao').one("click", function(){
            var $this = $(this).parent();
            $this.siblings("input").removeClass("hide").addClass("show").show();
            $this.removeClass("show").addClass("hide").hide();
        });
        $('#inpData').datetimepicker({
            language: 'pt-BR',
            icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
            }
        });

        $('#inpData').on("click", "input:first", function(){
            jQuery(this).parent().find("span").trigger("click");
        });
    }

    function loadImages(){
        $("img.lazy").not('.lazyloaded').lazyload({
            effect : "fadeIn" ,
            failure_limit : 4
        }).addClass('lazyloaded').bind('error',function(){
            console.log("aee....");
        });
    }

    loadImages();
    $(document).ajaxComplete(function() { loadImages(); });
});
