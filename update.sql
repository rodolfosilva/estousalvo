SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `nome`, `slug`, `published`) VALUES
(1, 'Adulto', 'adulto', 1),
(2, 'Animais', 'animais', 1),
(3, 'Animes', 'animes', 1),
(4, 'Automóveis', 'automoveis', 1),
(5, 'Bizarro', 'bizarro', 1),
(6, 'Celebridades', 'celebridades', 1),
(7, 'Cinema', 'cinema', 1),
(8, 'Cultura', 'cultura', 1),
(9, 'Curiosidades', 'curiosidades', 1),
(10, 'Dicas', 'dicas', 1),
(11, 'Downloads', 'downloads', 1),
(12, 'Entretenimento', 'entretenimento', 1),
(13, 'Esporte', 'esporte', 1),
(14, 'Games', 'games', 1),
(15, 'Gastronomia', 'gastronomia', 1),
(16, 'Humor', 'humor', 1),
(17, 'Incrível', 'incrivel', 1),
(18, 'Motocicletas', 'motocicletas', 1),
(19, 'Mulher', 'mulher', 1),
(20, 'Moda', 'moda', 1),
(21, 'Noticias', 'noticias', 1),
(22, 'Religião', 'religiao', 1),
(23, 'Sexy', 'sexy', 1),
(24, 'Tecnologia', 'tecnologia', 1),
(25, 'Tirinhas', 'tirinhas', 1),
(26, 'Utilidades', 'utilidades', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `imagens`
--

CREATE TABLE IF NOT EXISTS `imagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diretorio` varchar(100) NOT NULL,
  `arquivo` varchar(50) NOT NULL,
  `largura` varchar(45) NOT NULL,
  `altura` varchar(45) NOT NULL,
  `tamanho` varchar(45) NOT NULL,
  `options` text,
  `created` datetime NOT NULL,
  `tipo` varchar(15) NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Estrutura da tabela `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) NOT NULL,
  `imagem_id` int(11) NULL,
  `site_id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `url` varchar(255) NOT NULL,
  `slug` varchar(10) NOT NULL,
  `cliques` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `link_cliques`
--

CREATE TABLE IF NOT EXISTS `link_cliques` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link_id` int(11) NOT NULL,
  `origem` varchar(255) DEFAULT NULL,
  `user_agent` text NOT NULL,
  `ip` varchar(20) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Acionadores `link_cliques`
--
DROP TRIGGER IF EXISTS `adiciona_clique_ao_link`;
DELIMITER //
CREATE TRIGGER `adiciona_clique_ao_link` BEFORE INSERT ON `link_cliques`
 FOR EACH ROW BEGIN
    UPDATE links SET cliques = cliques + 1 WHERE id = NEW.link_id;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `remove_clique_do_link`;
DELIMITER //
CREATE TRIGGER `remove_clique_do_link` BEFORE DELETE ON `link_cliques`
 FOR EACH ROW BEGIN
    UPDATE links SET cliques = cliques - 1 WHERE id = OLD.link_id;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sites`
--

CREATE TABLE IF NOT EXISTS `sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `url` varchar(45) NOT NULL,
  `confirmacao` tinyint(1) NOT NULL,
  `token` varchar(36) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Acionadores `sites`
--
DROP TRIGGER IF EXISTS `bloqueia_token`;
DELIMITER //
CREATE TRIGGER `bloqueia_token` BEFORE UPDATE ON `sites`
 FOR EACH ROW BEGIN
IF NEW.url != OLD.url THEN
    UPDATE user_sites SET confirmacao = FALSE WHERE site_id = OLD.id;
END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(32) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_sites`
--

CREATE TABLE IF NOT EXISTS `user_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- 2013-08-01
ALTER TABLE `sites` DROP `confirmacao`, DROP `token`;
ALTER TABLE  `user_sites` ADD  `confirmacao` TINYINT( 1 ) NOT NULL DEFAULT  '0',
ADD  `token` VARCHAR( 35 ) NOT NULL;

--
-- Acionadores `user_sites`
--
DROP TRIGGER IF EXISTS `adiciona_token_ao_site`;
DROP TRIGGER IF EXISTS `adiciona_token_ao_user_site`;
DROP TRIGGER IF EXISTS `adiciona_token_ao_user_site`;
DELIMITER //
CREATE TRIGGER `adiciona_token_ao_user_site` BEFORE INSERT ON `user_sites`
 FOR EACH ROW SET NEW.token = UUID()
//
DELIMITER ;

-- 2013-08-15
ALTER TABLE  `links` CHANGE  `imagem_id`  `imagem_id` int(11) NULL;

-- 2013-08-27
ALTER TABLE  `links` ADD  `descricao` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `titulo`;


-- 2013-08-31
ALTER TABLE  `users` ADD  `token` VARCHAR( 32 ) NULL AFTER  `password`;
DROP TRIGGER IF EXISTS `remove_token_apos_mudar_senha`;
DELIMITER //
CREATE TRIGGER `remove_token_apos_mudar_senha` BEFORE UPDATE ON `users`
 FOR EACH ROW
IF
  NEW.nome != OLD.nome
  OR NEW.email != OLD.email
  OR NEW.username != OLD.username
  OR NEW.password != OLD.password
  OR NEW.modified != OLD.modified
  OR NEW.published != OLD.published
THEN
    SET NEW.token = null;
END IF
//
DELIMITER ;

-- 2013-09-02

--
-- Estrutura da tabela `visitas`
--

CREATE TABLE IF NOT EXISTS `visitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(20) NULL,
  `domain` varchar(45) NULL,
  `plataforma` varchar(50) NULL,
  `browser` varchar(100) NULL,
  `user_agent` varchar(255) NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 2013-09-05

CREATE OR REPLACE VIEW `view_sites_visitas` AS
SELECT
  `sites`.`id` AS `id`,
  `sites`.`titulo` AS `titulo`,
  `sites`.`url` AS `domain`,
  COUNT(0) AS `total_visitas`,
  `visitas`.`created` AS `created`
FROM
  `visitas` JOIN
  `sites` ON (`sites`.`url` = `visitas`.`domain` OR `sites`.`url` = CONCAT('www.', `visitas`.`domain`))
WHERE `sites`.`published` >= 1
GROUP BY `sites`.`id` , CONCAT(CAST(`visitas`.`created` AS DATE) , HOUR(`visitas`.`created`))
ORDER BY CONCAT(CAST(`visitas`.`created` AS DATE) , HOUR(`visitas`.`created`)) DESC , COUNT(0) DESC;

-- 2013-09-06
UPDATE `visitas` SET `created`= DATE_FORMAT(created,'%Y-%d-%m %H:%i:%s');

-- 2013-09-17
ALTER TABLE  `links` ADD  `post_date` DATETIME NOT NULL AFTER  `cliques`;
UPDATE `links` SET `post_date`=`created`;

-- 2013-09-26 : feature-9
CREATE TABLE IF NOT EXISTS `rankings` (
  `site_id` int(11) NOT NULL,
  `cliques` int(11) DEFAULT 0,
  `visitas` int(11) DEFAULT 0,
  `links` int(11) DEFAULT 0,
  `ranking` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DELIMITER $$
DROP PROCEDURE IF EXISTS atualiza_ranking_dos_sites$$
CREATE PROCEDURE atualiza_ranking_dos_sites()
BEGIN
    DECLARE v_finished INT(1) DEFAULT 0;
    DECLARE v_site_id INT(11) DEFAULT 0;
    DECLARE v_site_cliques INT(11) DEFAULT 0;
    DECLARE v_site_total_links INT(11) DEFAULT 0;
    DECLARE v_site_visitas INT(11) DEFAULT 0;
    DEClARE estatistica CURSOR FOR SELECT sites.id, (SELECT SUM(cliques) FROM links WHERE site_id = sites.id) AS cliques, (SELECT COUNT(*) FROM links WHERE site_id = sites.id) AS total_links, SUM(vsv.total_visitas) AS visitas FROM sites LEFT JOIN view_sites_visitas AS vsv ON vsv.id = sites.id  WHERE sites.published = 1 GROUP BY sites.id ORDER BY cliques DESC, SUM(vsv.total_visitas) DESC;
    -- declare NOT FOUND handler
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;
    SET @rank=0;
    START TRANSACTION;
    OPEN estatistica;
    gera_ranking: LOOP FETCH estatistica INTO v_site_id, v_site_cliques, v_site_total_links, v_site_visitas;
        IF v_finished = 1 THEN
            LEAVE gera_ranking;
        END IF;
        IF v_site_id IS NOT NULL THEN
            IF EXISTS(SELECT site_id FROM rankings WHERE site_id = v_site_id) THEN
                UPDATE rankings SET ranking=(@rank:=@rank+1), cliques = v_site_cliques, links = v_site_total_links, visitas = v_site_visitas, modified = NOW() WHERE site_id = v_site_id;
            ELSE
                INSERT INTO rankings (site_id, cliques, links, visitas, ranking, modified) VALUES (v_site_id, v_site_cliques, v_site_total_links, v_site_visitas, (@rank:=@rank+1), NOW());
            END IF;
        END IF;
    END LOOP gera_ranking;
    CLOSE estatistica;
    COMMIT;
END$$
DELIMITER ;

CREATE EVENT atualizar_ranking_30_minutos
ON SCHEDULE EVERY 30 MINUTE
COMMENT 'Evento para atualizar o ranking dos sites a cada 30 minutos'
DO CALL atualiza_ranking_dos_sites();

-- 2013-10-20 : feature-18
ALTER TABLE `users` ADD `facebook_id` BIGINT NULL AFTER `password`;

-- 2013-10-21 : Data no cadastro de novos criação dos sites
ALTER TABLE `sites` ADD `modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `url` , ADD `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `modified`;

-- 2013-11-28
ALTER TABLE `sites` ADD `imagem_id` int(11) NULL AFTER `id`;

-- 2013-12-12

CREATE OR REPLACE VIEW `view_sites_visitas` AS
SELECT
  `sites`.`id` AS `id`,
  `sites`.`titulo` AS `titulo`,
  `sites`.`url` AS `domain`,
  CONCAT(`imagens`.`diretorio`, `imagens`.`arquivo`) AS `site_img`,
  COUNT(0) AS `total_visitas`,
  `visitas`.`created` AS `created`
FROM
  `visitas` JOIN
  `sites` ON (`sites`.`url` = `visitas`.`domain` OR `sites`.`url` = CONCAT('www.', `visitas`.`domain`))
  LEFT JOIN `imagens` ON (`sites`.`imagem_id` = `imagens`.`id`)
WHERE `sites`.`published` >= 1
GROUP BY `sites`.`id` , CONCAT(CAST(`visitas`.`created` AS DATE) , HOUR(`visitas`.`created`))
ORDER BY CONCAT(CAST(`visitas`.`created` AS DATE) , HOUR(`visitas`.`created`)) DESC , COUNT(0) DESC;

-- #25 - 2013-12-16

--
-- Estrutura para tabela `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `artigo` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT  '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE  `links` ADD INDEX (  `categoria_id` ) ;
ALTER TABLE  `links` ADD INDEX (  `imagem_id` ) ;
ALTER TABLE  `links` ADD INDEX (  `site_id` ) ;
ALTER TABLE  `links` ADD UNIQUE (`slug`);
ALTER TABLE  `links` ADD INDEX (  `post_date` ) ;
ALTER TABLE  `links` ADD INDEX (  `published` ) ;
ALTER TABLE  `link_cliques` ADD INDEX (  `link_id` ) ;
ALTER TABLE  `sites` ADD INDEX (  `published` ) ;
ALTER TABLE  `sites` ADD INDEX (  `imagem_id` ) ;
ALTER TABLE  `users` ADD INDEX (  `username` ) ;
ALTER TABLE  `users` ADD INDEX (  `email` ) ;
ALTER TABLE  `users` ADD INDEX (  `facebook_id` ) ;
ALTER TABLE  `users` ADD INDEX (  `published` ) ;
ALTER TABLE  `user_sites` ADD INDEX (  `user_id` ) ;
ALTER TABLE  `user_sites` ADD INDEX (  `site_id` ) ;
ALTER TABLE  `user_sites` ADD INDEX (  `confirmacao` ) ;

OPTIMIZE TABLE
`categorias` ,
`imagens` ,
`links` ,
`link_cliques` ,
`rankings` ,
`sites` ,
`users` ,
`user_sites` ,
`visitas`;

INSERT INTO  `estousalvo`.`posts` (
`id` ,
`titulo` ,
`artigo` ,
`created` ,
`modified` ,
`published`
)
VALUES (
NULL ,  'Envie seu banner',  'Agora você pode enviar o banner do seu site no tamanho 230x40 para o <b><a href="http://estousalvo.com.br/" target="_blank"><span style="color: #3d85c6;">EstouSalvo</span></a></b>, ao invés de aparecer o thumb do seu site, irá aparecer a sua logomarca, no topo do nosso site, onde é exibido o últimos sites que enviaram visitas para o&nbsp;<b><a href="http://estousalvo.com.br/" target="_blank"><span style="color: #3d85c6;">EstouSalvo</span></a></b>.<br />
<br />
Para alterar o seu banner vá até o menu "Meus sites" e selecione o site que você deseja alterar o banner. A opção de alterar o banner esta no mesmo formulário da alteração dos dados do site.', '2013-12-13 07:10:22',  '2013-12-13 07:10:22',  '1'
);

-- 2014-01-15

ALTER TABLE `visitas` DROP `user_agent`;
ALTER TABLE `link_cliques` DROP `user_agent`;
ALTER TABLE  `posts` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  `posts` CHANGE  `titulo`  `titulo` VARCHAR( 100 ) NOT NULL ,
                     CHANGE  `artigo`  `artigo` TEXT NOT NULL;
