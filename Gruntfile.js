'use strict';

module.exports = function( grunt ) {
    grunt.initConfig( {
        shell: {
            cleanPublic: {
                command: 'rm public/[^upload]* -Rf'
            }
        },
        uglify : {
            options : {
                mangle : false
            },
            app : {
                files : {
                    './public/js/app.min.js' : [
                        './assets/js/jquery.min.js',
                        './assets/js/bootstrap.min.js',
                        './assets/js/jquery.lazyload.min.js',
                        './assets/js/jquery.form.min.js',
                        './assets/js/jquery.mousewheel.js',
                        './assets/js/jquery.jscrollpane.min.js',
                        './assets/js/app/feedback.js',
                        './assets/js/app/estousalvo.js',
                        './assets/js/scripts.js'
                    ],
                    './public/js/add_link.min.js' : [
                        './assets/js/jquery.Jcrop.min.js',
                        './assets/js/moment.min.js',
                        './assets/js/bootstrap-datetimepicker.min.js',
                        './assets/js/locales/bootstrap-datetimepicker.pt-BR.js',
                        './assets/js/jquery.nouislider.min.js',
                        './assets/js/crop.js',
                        './assets/js/app/addLink.js',
                    ]
                }
            }
        },
        less: {
            app: {
                options: {
                    compress: true,
                    cleancss: true, // Caso dê algum problema, terei que remover isto.
                    yuicompress: true,
                    report: 'min',
                    optimization: 3
                },
                files: {
                    './public/css/app.min.css': './assets/css/styles.less',
                    './public/css/add_link.min.css': [
                        './assets/css/jquery.Jcrop.min.css',
                        './assets/css/less/crop.less',
                        './assets/css/bootstrap-datetimepicker.min.css',
                    ]
                }
            },
            minify: {
                options: {
                    cleancss: true,
                    report: 'min'
                },
                files: {
                    './public/css/add_link.min.css': './public/css/add_link.min.css',
                    './public/css/app.min.css': './public/css/app.min.css'
                }
            }
        },
        cssmin: {
            combine: {
                options: {
                    keepSpecialComments: 0,
                    report: 'min'
                },
                files: {
                    './public/css/add_link.min.css': './public/css/add_link.min.css',
                    './public/css/app.min.css': './public/css/app.min.css'
                }
            }
        },
        copy: {
            fonts: {
                expand: true,
                src: './assets/fonts/*',
                dest: './public/fonts/',
                flatten: true,
            },
            js_ie: {
                expand: true,
                src: './assets/js/html5shiv.js',
                dest: './public/js/',
                flatten: true,
            },
            index: {
                expand: true,
                src: './assets/index.html',
                dest: './public/',
                flatten: true,
            },
            banners: {
                expand: true,
                src: './assets/banners/*',
                dest: './public/banners/',
                flatten: true,
            },
            imagens: {
                expand: true,
                src: './assets/img/*',
                dest: './public/img/',
                flatten: true,
            }
        },
        imagemin: {
            banners: {
                options: {
                  cache: false
                },
                files: [{
                    expand: true,
                    cwd: './assets/banners/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: './public/banners/'
                }]
            },
            imagens: {
                options: {
                  cache: false
                },
                files: [{
                    expand: true,
                    cwd: './assets/img/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: './public/img/'
                }]
            }
        }
    } );
    grunt.loadNpmTasks( 'grunt-shell' );
    grunt.loadNpmTasks( 'grunt-contrib-imagemin' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-less' );
    grunt.loadNpmTasks( 'grunt-contrib-copy' );
    grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
    grunt.registerTask( 'default', ['uglify', 'less', 'cssmin', 'copy', 'imagemin' ] );
};
